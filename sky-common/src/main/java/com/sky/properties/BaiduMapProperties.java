package com.sky.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "sky.baidu")
public class BaiduMapProperties {
    private String ak; //ak
    private String geoCoderUrl; //计算地址坐标
    private String drivingUrl;//计算两地的距离
}
