package com.sky.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sky.constant.MessageConstant;
import com.sky.exception.BusinessException;
import com.sky.properties.BaiduMapProperties;
import com.sky.result.Location;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;

@Data
@AllArgsConstructor
public class BaiduMapUtil {
    private BaiduMapProperties baiduMapProperties;

    //得到地址坐标
    public Location getAddressGeo(String address){

        String url = baiduMapProperties.getGeoCoderUrl();

        HashMap<String, String> map = new HashMap<>();
        map.put("address",address);
        map.put("output","json");
        map.put("ak",baiduMapProperties.getAk());
        String result = HttpClientUtil.doGet(url, map);

        JSONObject resultObject = JSON.parseObject(result);
        Integer status = resultObject.getInteger("status");
        if(status != 0){
            throw new BusinessException(MessageConstant.ADDRESS_PARSE_ERROR);
        }

        JSONObject locationObject = resultObject.getJSONObject("result").getJSONObject("location");
        String lng = locationObject.get("lng").toString();
        String lat = locationObject.get("lat").toString();

       return new Location(lat,lng);
    }

    public Integer getDistance(Location origin,Location destination){
        String url = "https://api.map.baidu.com/directionlite/v1/driving?";

        HashMap<String, String> params = new HashMap<>();
        params.put("origin",origin.getLatLnt());
        params.put("destination",destination.getLatLnt());
        params.put("ak",baiduMapProperties.getAk());
        String result = HttpClientUtil.doGet(url, params);

        JSONObject resultObject = JSON.parseObject(result);
        Integer status = resultObject.getInteger("status");
        if(status != 0) {
            throw new BusinessException(MessageConstant.ADDRESS_PARSE_ERROR);
        }

        JSONArray routesObject  = resultObject.getJSONObject("result").getJSONArray("routes");
        JSONObject jsonObject = routesObject.getJSONObject(0);
        Integer distance = jsonObject.getInteger("distance");

        return distance;
    }

}
