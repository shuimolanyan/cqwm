package com.sky.constant;

public class RedisConstant {
    public static final String SHOP_STATUS_KEY = "shop:status:key:";

    public static final String CACHE_DISH_KEY = "dish:cache:";
}
