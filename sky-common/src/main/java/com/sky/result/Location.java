package com.sky.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Location {

    private String lng;
    private String lat;

    public String getLatLnt(){
        return lng + "," + lat;
    }
}
