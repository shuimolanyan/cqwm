package com.sky.interceptor;

import com.sky.context.BaseContext;
import com.sky.properties.JwtProperties;
import com.sky.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.sky.constant.JwtClaimsConstant.EMP_ID;
import static com.sky.constant.JwtClaimsConstant.USER_ID;

@Component
@Slf4j
public class UserLoginInterceptor implements HandlerInterceptor {
    @Autowired
    private JwtProperties jwtProperties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //1.获取令牌
        String token = request.getHeader(jwtProperties.getUserTokenName());

        //2.令牌为空，拦截 响应状态码401
        if(!StringUtils.hasLength(token)){
            log.error("令牌为空");
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        }

        //3.解析令牌，错误， 拦截 响应状态码401 将登录员工的id存入，方便后续使用：比如在修改用户时，要在数据库中标注是谁修改的
        //还保证了线程的安全，保证对某一个值得修改时同一个人
        try {
            Claims claims = JwtUtil.parseJWT(jwtProperties.getUserSecretKey(), token);
            Long userId = Long.valueOf(claims.get(USER_ID).toString());
            BaseContext.setCurrentId(userId);

        } catch (Exception e) {
            log.error("令牌解析错误");
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        }
        //4.放行
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        BaseContext.removeCurrentId();
    }
}
