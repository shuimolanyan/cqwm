package com.sky.controller.admin;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.dto.SetmealDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 分类
 */
@Api(tags = "分类接口")
@RestController
@RequestMapping("/admin/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 分页查询
     * @param categoryPageQueryDTO
     * @return
     */
    @ApiOperation(("分类分页查询"))
    @GetMapping("/page")
    public Result page(CategoryPageQueryDTO categoryPageQueryDTO){
        PageResult pageResult= categoryService.page(categoryPageQueryDTO);
        return Result.success(pageResult);
    }

    /**
     * 根据id查询分类
     * @param id
     * @return
     */
    @ApiOperation("根据id查询分类")
    @GetMapping("/{id}")
    public Result findCategoryId(@PathVariable Long id){
        Category category = categoryService.findCategoryById(id);
        return Result.success(category);
    }

    /**
     * 修改分类
     * @param categoryDTO
     * @return
     */
    @ApiOperation("修改分类")
    @PutMapping
    public Result updateCategory(@RequestBody CategoryDTO categoryDTO){
        categoryService.updateCategory(categoryDTO);
        return Result.success();
    }

    /**
     * 禁用/启用
     * @param status
     * @param id
     * @return
     */
    @ApiOperation("禁用/启用")
    @PutMapping("/status/{status}/{id}")
    public Result updateStatus(@PathVariable Integer status, @PathVariable Long id){
        categoryService.updateStatus(status,id);
        return Result.success();
    }

    /**
     * 新增分类
     * @param categoryDTO
     * @return
     */
    @ApiOperation("新增分类")
    @PostMapping
    public Result insetCategory(@RequestBody CategoryDTO categoryDTO){
        categoryService.insetCategory(categoryDTO);
        return Result.success();
    }

    /**
     * 根据id删除分类
     * @param id
     */
    @ApiOperation("根据id删除分类")
    @DeleteMapping("/{id}")
    public Result deleteCategory(@PathVariable Long id){
        categoryService.deleteCategory(id);
        return Result.success();
    }


    @ApiOperation("根据类型查询分类")
    @GetMapping("/list")
    public Result findCategoryByType(CategoryDTO categoryDTO){
        List<Category> categoryList = categoryService.findCategoryByType(categoryDTO);
        return Result.success(categoryList);
    }


}

