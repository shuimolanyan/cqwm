package com.sky.controller.admin;

import com.sky.dto.OrdersCancelDTO;
import com.sky.dto.OrdersConfirmDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersRejectionDTO;
import com.sky.entity.OrderDetail;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.AdminOrderService;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/admin/order")
@Api(tags = "后台订单模块")
public class OrderController {
    @Autowired
    private AdminOrderService adminOrderService;

    /**
     * 条件分页查询
     */
    @GetMapping("/conditionSearch")
    public Result<PageResult> page(OrdersPageQueryDTO ordersPageQueryDTO){
        PageResult pageResult = adminOrderService.list(ordersPageQueryDTO);
        return Result.success(pageResult);
    }

    /**
     * 统计各个状态的订单数量
     */
    @GetMapping("/statistics")
    public Result<OrderStatisticsVO> countOderStatusNumber(){
        OrderStatisticsVO orderStatisticsVO = adminOrderService.countOderStatusNumber();
        return Result.success(orderStatisticsVO);
    }

    /**
     * 查询订单详情
     */
    @GetMapping("/details/{id}")
    public Result<OrderVO> getOrderDetailById(@PathVariable Long id){
        OrderVO orderVO = adminOrderService.findOrderDetailById(id);
        return Result.success(orderVO);
    }

    /**
     * 接单
     */
    @PutMapping("/confirm")
    public Result receiveOrder(@RequestBody OrdersConfirmDTO confirmDTO){
        adminOrderService.receiveOrder(confirmDTO);
        return Result.success();
    }

    /**
     * 拒单
     */
    @PutMapping("/rejection")
    public Result rejectOrder(@RequestBody OrdersRejectionDTO ordersRejectionDTO){
        adminOrderService.rejectOrder(ordersRejectionDTO);
        return Result.success();
    }

    /**
     * 取消订单
     */
    @PutMapping("/cancel")
    private Result cancelOrder(@RequestBody OrdersCancelDTO ordersCancelDTO){
        adminOrderService.cancelOrder(ordersCancelDTO);
        return Result.success();
    }

    /**
     * 派送订单
     */
    @PutMapping("/delivery/{id}")
    public Result diliveryOrder(@PathVariable Long id){
        adminOrderService.deliveryOrder(id);
        return Result.success();
    }

    /**
     * 完成订单
     */
    @PutMapping("/complete/{id}")
    public Result successOrder(@PathVariable Long id){
        adminOrderService.susccessOrder(id);
        return Result.success();
    }
}
