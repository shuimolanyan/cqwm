package com.sky.controller.admin;

import com.sky.result.Result;
import com.sky.service.ShopService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "商店状态")
@RestController
@RequestMapping("/admin/shop")
@Slf4j
public class ShopController {

    @Autowired
    private ShopService shopService;

    @GetMapping("/status")
    public Result updateStatus(){
        Integer status = shopService.getShopStatus();
        return Result.success(status);
    }

    @PutMapping("/{status}")
    public Result update(@PathVariable Integer status){
        shopService.setStatus(status);
        return Result.success();
    }
}
