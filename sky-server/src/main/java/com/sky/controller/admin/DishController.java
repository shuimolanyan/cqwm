package com.sky.controller.admin;

import com.sky.constant.RedisConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@Slf4j
@RequestMapping("/admin/dish")
public class DishController {

    @Autowired
    private DishService dishService;

    @Autowired
    private RedisTemplate<Object,Object> redisTemplate;

    @ApiOperation("新增菜品")
    @PostMapping
    public Result save(@RequestBody DishDTO dishDTO){
        log.info("新增菜品，前端传来的数据：{}",dishDTO);

        dishService.save(dishDTO);

        //删除缓存中的菜品数据，只删除该分类对应下的全部菜品
        clearCache(dishDTO.getCategoryId().toString());
        return Result.success();
    }

    /**
     * 分页条件查询
     * @param dishPageQueryDTO
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public Result<PageResult> page(DishPageQueryDTO dishPageQueryDTO){
        PageResult pageResult = dishService.page(dishPageQueryDTO);
        return Result.success(pageResult);
    }


    /**
     * 根据id进行批量删除
     * @param ids
     * @return
     */
    @ApiOperation("删除菜品")
    @DeleteMapping
    public Result delete(@RequestParam List<Long> ids){
        dishService.deleteDishByIds(ids);

        //删除redis中的缓存数据，全部删除，因为删除菜品可能是批量删除
        clearCache("*");
        return Result.success();
    }



    /**
     * 根据id查询数据
     * @param id
     * @return
     */
    @ApiOperation("根据id查询菜品")
    @GetMapping("/{id}")
    public Result<DishVO> getDishById(@PathVariable Long id){
        DishVO dishVO = dishService.findDishById(id);
        return Result.success(dishVO);
    }


    @ApiOperation("修改菜品信息")
    @PutMapping
    public Result updateDish(@RequestBody DishDTO dishDTO){
        dishService.updateDish(dishDTO);

        //修改分类的话设计多个分类，全部清理缓存
        clearCache("*");
        return Result.success();
    }

    @ApiOperation("停售/起售")
    @PutMapping("/status/{status}/{id}")
    public Result updateStatus(@PathVariable Integer status, @PathVariable Long id){
        dishService.updateStatus(status,id);

        //清理全部
        clearCache("*");
        return Result.success();
    }

    /**
     * 根据类型id查询菜品
     */
    @GetMapping("/list")
    public Result findDishByCategoryId(DishDTO dishDTO){
        List dishList = dishService.findDishByCategoryIdOrName(dishDTO);
        return Result.success(dishList);
    }

    //清理redis中的缓存
    private void clearCache(String suffix) {
        Set<Object> keys = redisTemplate.keys(RedisConstant.CACHE_DISH_KEY + suffix);
        redisTemplate.delete(keys);
    }
}

