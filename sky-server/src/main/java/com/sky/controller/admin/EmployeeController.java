package com.sky.controller.admin;

import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.properties.JwtProperties;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.EmployeeService;
import com.sky.utils.JwtUtil;
import com.sky.vo.EmployeeLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static com.sky.constant.JwtClaimsConstant.EMP_ID;

/**
 * 员工管理Controller
 */
@Slf4j
@Api(tags = "员工接口")
@RestController
@RequestMapping("/admin/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private JwtProperties jwtProperties;

    /**
     * 员工登录
     * @param employeeLoginDTO
     * @return
     */
    @ApiOperation("登录方法")
    @PostMapping("/login")
    public Result login(@RequestBody EmployeeLoginDTO employeeLoginDTO){
        log.info("员工登录，前端的传来的参数：{}",employeeLoginDTO);
        //1.根据username得到员工信息
        Employee employee = employeeService.login(employeeLoginDTO);

        //2.生成Jwt令牌
        Map<String,Object> clamis = new HashMap<>();
        clamis.put(EMP_ID,employee.getId());
        String jwt = JwtUtil.createJWT(jwtProperties.getAdminSecretKey(), jwtProperties.getAdminTtl(), clamis);

        //3.封装数据EmployeeLoginVO
        EmployeeLoginVO employeeLoginVO = EmployeeLoginVO.builder()
                .id(employee.getId())
                .name(employee.getName())
                .userName(employee.getUsername())
                .token(jwt).build();

        return Result.success(employeeLoginVO);
    }

    /**
     * 新增员工
     * @return
     */
    @ApiOperation("新增员工方法")
    @PostMapping
    public Result save(@RequestBody EmployeeDTO employeeDTO){
        log.info("新增员工：{}",employeeDTO);
        employeeService.save(employeeDTO);
        return Result.success();
    }

    /**
     * 分页查询
     * @param employeePageQueryDTO
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public Result page(EmployeePageQueryDTO employeePageQueryDTO){
        log.info("分页查询");
        PageResult PageResult = employeeService.page(employeePageQueryDTO);
        return Result.success(PageResult);
    }

    /**
     * 禁用/启用
     * @param status
     * @param id
     * @return
     */
    @ApiOperation("禁用/启用员工")
    @PutMapping("/status/{status}/{id}")
    public Result EnableOrDisable(@PathVariable Integer status,@PathVariable Long id){
        employeeService.enableOrDisable(status,id);
        return Result.success();
    }

    @ApiOperation("根据id查询员工")
    @GetMapping("/{id}")
    public Result findEmployeeById(@PathVariable Integer id){
        Employee employee = employeeService.findEmployeeById(id);

        return Result.success(employee);
    }

    @ApiOperation("修改员工")
    @PutMapping
    public Result update(@RequestBody EmployeeDTO employeeDTO){
        employeeService.updateEmployee(employeeDTO);
        return Result.success();
    }

    @ApiOperation("修改密码")
    @PutMapping("/editPassword")
    public Result updatePassword(@RequestBody PasswordEditDTO passwordEditDTO){
        employeeService.updatePassword(passwordEditDTO);
        return Result.success();
    }
}
