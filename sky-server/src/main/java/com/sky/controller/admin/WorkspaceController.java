package com.sky.controller.admin;

import com.sky.result.Result;
import com.sky.service.WokspaceService;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = "工作台接口")
@RestController
@RequestMapping("/admin/workspace")
public class WorkspaceController {

    @Autowired
    private WokspaceService wokspaceService;

    /**
     * 查询今日营业数据
     * @return
     */
    @GetMapping("/businessData")
    public Result<BusinessDataVO> getBusinessNowDay(){
        BusinessDataVO businessDataVO = wokspaceService.findBusinessData();

        return Result.success(businessDataVO);
    }

    /**
     * 查询今日订单数据
     */
    @GetMapping("/overviewOrders")
    private Result<OrderOverViewVO> orderOverViewVO(){
       OrderOverViewVO orderOverViewVO =  wokspaceService.findOrderDataToday();
       return Result.success(orderOverViewVO);
    }

    /**
     * 查询菜品总览
     */
    @GetMapping("/overviewDishes")
    public Result<SetmealOverViewVO> overViewDishes(){
        SetmealOverViewVO setmealOverViewVO = wokspaceService.findOverDisherView();
        return Result.success(setmealOverViewVO);
    }

    /**
     * 查询套餐总览
     */
    @GetMapping("/overviewSetmeals")
    public Result<SetmealOverViewVO> overViewSetmeal(){
        SetmealOverViewVO setmealOverViewVO = wokspaceService.findOverSetmealView();

        return Result.success(setmealOverViewVO);
    }
}
