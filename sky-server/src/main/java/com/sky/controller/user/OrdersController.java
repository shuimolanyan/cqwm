package com.sky.controller.user;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.OrdersDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersSubmitDTO;
import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.OrdersService;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@Slf4j
@Api(tags = "C端下单")
@RequestMapping("/user/order")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;


    /**
     * 下单
     * @param ordersSubmitDTO
     * @return
     */
    @PostMapping("/submit")
    public Result<OrderSubmitVO> ordersSumbit(@RequestBody OrdersSubmitDTO ordersSubmitDTO) throws IOException {
        OrderSubmitVO orderSubmitVO = ordersService.submitOrders(ordersSubmitDTO);
        return Result.success(orderSubmitVO);
    }

    /**
     * 历史订单查询
     */
    @GetMapping("/historyOrders")
    public Result<PageResult> findHistoryOrders(OrdersPageQueryDTO ordersPageQueryDTO){
        PageResult pageResult = ordersService.findHistoryOrders(ordersPageQueryDTO);
        return Result.success(pageResult);
    }

    /**
     * 查询订单详情
     */
    @GetMapping("/orderDetail/{id}")
    public Result<OrderVO> getOrderDetailById(@PathVariable Long id){
        OrderVO orderVO = ordersService.findOrderDeatilById(id);
        return Result.success(orderVO);
    }

    /**
     * 取消订单
     */
    @PutMapping("/cancel/{id}")
    public Result cancelOrder(@PathVariable Long id){
        ordersService.cancelOrder(id);
        return Result.success();
    }

    /**
     * 再来一单
     */
    @PostMapping("/repetition/{id}")
    public Result increOrders(@PathVariable Long id){
        ordersService.onceBy(id);
        return Result.success();
    }

    /**
     * 催单
     */
    @GetMapping("/reminder/{id}")
    public Result reminderOrder(@PathVariable Long id) throws IOException {
        ordersService.reminderOrder(id);
        return Result.success();
    }
}
