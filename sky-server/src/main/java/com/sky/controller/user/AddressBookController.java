package com.sky.controller.user;

import com.sky.dto.AddressBookDTO;
import com.sky.entity.AddressBook;
import com.sky.result.Result;
import com.sky.service.AddressBookService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user/addressBook")
@Api(tags = "地址模块")
public class AddressBookController {

    @Autowired
    private AddressBookService addressBookService;


    /**
     * 新增地址
     */
    @PostMapping
    public Result addAddress(@RequestBody AddressBookDTO addressBookDTO){
        log.info("新增地址，前端传来的数据：{}",addressBookDTO);
        addressBookService.addAddress(addressBookDTO);
        return Result.success();
    }

    /**
     * 查询当前用户的地址列表
     */
    @GetMapping("/list")
    public Result<List<AddressBook>> list(){
        log.info("查询当前用户下的地址列表");
        List<AddressBook> addressBooks = addressBookService.list();
        return Result.success(addressBooks);
    }

    /**
     * 设置默认地址
     */
    @PutMapping("/default")
    public Result updateAddressIsDefault(@RequestBody AddressBookDTO addressBookDTO){
        log.info("将地址{}设为默认",addressBookDTO);
        addressBookService.updateAddressIsDefault(addressBookDTO);
        return Result.success();
    }

    /**
     * 查询默认地址的用户
     */
    @GetMapping("/default")
    public Result<AddressBook> findDefaultAddress(){
        log.info("根据默认地址查询用户");
        List<AddressBook> addressBooks = addressBookService.findDefaultAddress();
        AddressBook addressBook = addressBooks.get(0);
        return Result.success(addressBook);
    }

    /**
     * 根据id查询地址
     */
    @GetMapping("/{id}")
    public Result<AddressBook> findAddressById(@PathVariable Long id){
        log.info("根据{}查询地址信息",id);
        AddressBook addressBook = addressBookService.findAddressById(id);
        return Result.success(addressBook);
    }

    /**
     * 根据id修改地址信息
     */
    @PutMapping
    public Result updateAddress(@RequestBody AddressBookDTO addressBookDTO){
        log.info("根据{}修改地址信息",addressBookDTO.getId());
        addressBookService.updateAddressById(addressBookDTO);
        return Result.success();
    }

    /**
     * 根据id删除地址
     */
    @DeleteMapping
    public Result deleteAddress(Long id){
        log.info("根据{}删除地址",id);
        addressBookService.deleteAddress(id);
        return Result.success();
    }
}

