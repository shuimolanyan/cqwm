package com.sky.controller.user;

import com.sky.constant.JwtClaimsConstant;
import com.sky.dto.CategoryDTO;
import com.sky.dto.DishDTO;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.Category;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.User;
import com.sky.properties.JwtProperties;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import com.sky.service.DishService;
import com.sky.service.SetmealService;
import com.sky.service.UserService;
import com.sky.utils.JwtUtil;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;
import com.sky.vo.UserLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@Api(tags = "用户管理接口")
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtProperties jwtProperties;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SetmealService setmealService;

    @Autowired
    private DishService dishService;

    /**
     * C端用户登录，若是第一次则保存到user表
     * @param userLoginDTO
     * @return
     */
    @PostMapping("/user/login")
    @ApiOperation("用户登录")
    public Result<UserLoginVO> login(@RequestBody UserLoginDTO userLoginDTO){
        //1.C端登录，若是第一次就保存
        User user = userService.login(userLoginDTO);

        //2.生成令牌
        HashMap<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.USER_ID,user.getId());
        String jwt = JwtUtil.createJWT(jwtProperties.getUserSecretKey(), jwtProperties.getUserTtl(), claims);

        //3.响应
        UserLoginVO userLoginVO = UserLoginVO.builder().id(user.getId()).openid(user.getOpenid()).token(jwt).build();
        return Result.success(userLoginVO);
    }

    /**
     * 根据（条件）查询分类列表
     */
    @GetMapping("/category/list")
    public Result<List<Category>> findCategroyByType(Integer type){
        List<Category> categoryList = categoryService.findCategoryByTypeOnC(type);
        return Result.success(categoryList);
    }

    /**
     * 根据分类id查询套餐
     */
    @GetMapping("/setmeal/list")
    private Result<List<Setmeal>> findSetmealByCategoryId(Long categoryId){
        List<Setmeal> setmealList = categoryService.findSetmealByCategoryId(categoryId);
        return Result.success(setmealList);
    }

    /**
     * 根据套餐id查询套餐中的菜品
     */
    @GetMapping("/setmeal/dish/{id}")
    private Result<List<DishItemVO>> findDishFromSetmealById(@PathVariable Long id){
       List<DishItemVO> dishItemVOS =  setmealService.findDishFromSetmealById(id);
       return Result.success(dishItemVOS);
    }

    /**
     * 根据分类id查询菜品及其口味
     */
    @GetMapping("/dish/list")
    public Result<List<DishVO>> findDishByCategoryId(Long categoryId){

        List<DishVO> dishVOS = dishService.findDishByCategoryId(categoryId);
        return Result.success(dishVOS);
    }
}
