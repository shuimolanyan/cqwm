package com.sky.controller.user;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import com.sky.service.ShoppingCartService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user/shoppingCart")
@Api(tags = "购物车模块")
public class ShoppingCatController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 添加商品到购物车
     * @param shoppingCartDTO
     * @return
     */
    @PostMapping("/add")
    public Result addShop(@RequestBody ShoppingCartDTO shoppingCartDTO){
        log.info("添加商品到购物车中,传来的基本商品参数{}",shoppingCartDTO);
        shoppingCartService.addShopToCart(shoppingCartDTO);
        return Result.success();
    }

    /**
     * 查看购物车
     */
    @GetMapping("/list")
    public Result<List<ShoppingCart>> list(){
        log.info("查询购物车中的商品");
        List<ShoppingCart> shoppingCarts = shoppingCartService.findShopOnCart();
        return Result.success(shoppingCarts);
    }

    /**
     * 删除购物车一个商品
     */
    @PostMapping("/sub")
    public Result subShopFromCart(@RequestBody ShoppingCartDTO shoppingCartDTO){
        shoppingCartService.subShopFromCart(shoppingCartDTO);
        return Result.success();
    }

    /**
     * 清空购物车
     */
    @DeleteMapping("/clean")
    public Result cleanCart(){
        shoppingCartService.clean();
        return Result.success();
    }
}
