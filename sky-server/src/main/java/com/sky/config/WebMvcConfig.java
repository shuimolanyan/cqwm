package com.sky.config;

import com.sky.interceptor.AdminLoginInterceptor;
import com.sky.interceptor.UserLoginInterceptor;
import com.sky.json.JacksonObjectMapper;
import com.sky.properties.AliOssProperties;
import com.sky.properties.BaiduMapProperties;
import com.sky.utils.AliOssUtil;
import com.sky.utils.BaiduMapUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import java.util.List;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private AdminLoginInterceptor adminLoginInterceptor;

    @Autowired
    private UserLoginInterceptor userLoginInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(adminLoginInterceptor).addPathPatterns("/admin/**")
                .excludePathPatterns("/admin/employee/login");

        registry.addInterceptor(userLoginInterceptor).addPathPatterns("/user/**")
                .excludePathPatterns("/user/user/login","/user/shop/status");
    }

    /**
     * 消息转换器，spring默认的日期转换器不满足我们的需求，所以需要我们扩展覆盖它，它底层是使用jackson，最主要的是ObjectMapping
     * ObjectMapper：ObjectMapper是Jackson库中的一个核心类，它提供了将Java对象序列化成JSON格式
     * new JacksonObjectMapper()里面：在Jackson库中，SimpleModule可以通过注册其子类的方式来实现扩展
     *                               通过SimpleModule的扩展方式，我们可以轻松地实现自定义的序列化和反序列化逻辑
     * 将扩展的Jackson注入到消息转换器中进行扩展，随后将整个消息转换器的优先级提高
     *
     *
     * 我们想要去掉时间格式中的t，首先我们要知道，controller是提供@responseBody将java对象转账json格式，而springboot框架
     * 底层是使用的jackson，而它里面的转时间json格式时系列化后自带t的，我们想要去掉t，首先要知道jackSon底层一个主要的类ObjectMapper
     * 它提供了将Java对象序列化成JSON格式，但它的序列化方式自带了t，所以我们需要对其序列化方式进行扩展，此处我们使用SimpleModule
     * 的扩展方式，它是javaweb提供的一个轻量级框架，通过SimpleModule的扩展方式，我们可以轻松地实现自定义的序列化和反序列化逻辑
     * 随后将我们自定义的序列化扩展注入到消息转换器中，最后将此消息转换器提高到最优（消息转换器不止一个，所以需要提供优先级）
     * @param converters
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setObjectMapper(new JacksonObjectMapper());

        converters.add(0,messageConverter);
    }

    /**
     * 将aliyunoos交给容器管理
     * @param aliOssProperties
     * @return
     */
    @Bean
    public AliOssUtil aliOssUtil(AliOssProperties aliOssProperties){

        return new AliOssUtil(aliOssProperties.getEndpoint(),aliOssProperties.getAccessKeyId(),
                aliOssProperties.getAccessKeySecret(),aliOssProperties.getBucketName());
    }

    /**
     * 将计算距离的百度util交给容器管理
     * @param baiduMapProperties
     * @return
     */
    @Bean
    public BaiduMapUtil baiduMapUtil(BaiduMapProperties baiduMapProperties){
        return new BaiduMapUtil(baiduMapProperties);
    }

    /**
     * ServerEndpointExporter类的作用是，会扫描所有的服务器端点，把带有  @ServerEndpoint 注解的所有类都添加进来
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
        return new ServerEndpointExporter();
    }
}
