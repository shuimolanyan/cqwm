package com.sky.mapper;

import com.sky.Aspect.AutoFile;
import com.sky.dto.CategoryDTO;
import com.sky.entity.Category;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

@Mapper
public interface CategoryMapper {
    //分页查询
    List<Category> list(Category category);

    //修改分类信息
    @AutoFile(OperationType.UPDATE)
    void update(Category category);

    //新增分类
    @AutoFile(OperationType.INSERT)
    void insert(Category category);

    //删除分类
    void delete(Long id);


    List<Category> findCategoryByTypeOnC(Integer type);

    @Select("select * from setmeal where category_id = #{id}  and status = 1")
    List<Setmeal> findSetmealByCategoryId(Long id);

    @Select("select * from category")
    List<Category> listAll();

}
