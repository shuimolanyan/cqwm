package com.sky.mapper;

import com.sky.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetMealDishMapper {
    //根据dishid计算在几套餐中，
    List<Long> countDishInSetmeal(List<Long> ids);

    //在菜品套餐关联表中批量添加菜品
    void addMutipleDishInSetmealDish(List<SetmealDish> setmealDishes);

    //根据套餐id删除套餐菜品关联表中的数据
    void deleteSetmealWithDishByIds(List<Long> ids);

    //根据套餐id查询套餐菜品关系表中的菜品
    @Select("select * from setmeal_dish where setmeal_id = #{id}")
    List<SetmealDish> findDishFormDishSetmeal(Long id);

}
