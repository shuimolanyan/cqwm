package com.sky.mapper;

import com.sky.entity.OrderDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrderDetailMapper {

    //批量添加数据到订单详情表
    void insert(List<OrderDetail> orderDetails);



}
