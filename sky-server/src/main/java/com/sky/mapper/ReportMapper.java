package com.sky.mapper;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.SalesReportDTO;
import com.sky.dto.TurnoverReportDTO;
import com.sky.dto.UserReportDTO;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.OrderReportVO;
import com.sky.vo.TurnoverReportVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface ReportMapper {

    //统计销售额
    List<TurnoverReportDTO> selectDataInTime(LocalDateTime beginTime, LocalDateTime endTime, int status);

    //统计用户数据
    List<UserReportDTO> selectUserDataInTime(LocalDateTime beginTime, LocalDateTime endTime);

    //统计订单总数
    List<OrderReportDTO> selectOrderDataInTime(LocalDateTime beginTime, LocalDateTime endTime, Integer status);

    //统计排名前十的菜品
    List<SalesReportDTO> selectTop10DishDataInTime(LocalDateTime beginTime, LocalDateTime endTime, Integer status);

    @Select("select  count(distinct user_id) from orders where order_time between #{begin} and #{end } and status = #{status}")
    Integer selectOrderDataInTimeByUserId(LocalDateTime begin, LocalDateTime end, Integer status);

    //查询时间段内的所以数据
    BusinessDataVO selectData(LocalDateTime begin, LocalDateTime end);

    //统计新增用户数量
    Integer countUser(LocalDateTime begin, LocalDateTime end);
}
