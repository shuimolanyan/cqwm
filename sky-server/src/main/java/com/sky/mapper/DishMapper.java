package com.sky.mapper;

import com.sky.Aspect.AutoFile;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealOverViewVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishMapper {
    //根据分类id or name查询菜品

    List<Dish> findDishByCategoryIdOrName(DishDTO dishDTO);

    //新增菜品
    @Options(useGeneratedKeys = true,keyProperty = "id")
    @AutoFile(OperationType.INSERT)
    @Insert("insert into dish (name, category_id, price, image, description, create_time, update_time, create_user, update_user) VALUES " +
            "(#{name},#{categoryId},#{price},#{image},#{description},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void insert(Dish dish);

    //分页条件查询
    List<DishVO> list(DishPageQueryDTO dishPageQueryDTO);

    //根据ids查询起售状态的菜品
    Long countEnableDishByIds(List<Long> ids);

    //根据ids删除菜品
    void deleteDishByIds(List<Long> ids);

    //根据id查询菜品
    @Select("select * from dish where id = #{id}")
    Dish findDishById(Long id);

    //修改菜品信息
    @AutoFile(OperationType.UPDATE)
    void updateDish(Dish dish);

    //根据分类id查询菜品

    //根据分类id查询起售的菜品信息
    @Select("select * from dish where category_id = #{id} and status = 1")
    List<Dish> findDishByCateoryId(Long categoryId);

    //查询起售停售的商品
    @Select("select count(*) from dish where status = #{status}")
    Integer countDishesIsNotEnable(int status);

    @Select("select\n" +
            "      sum(case status when 0 then 1 else 0 end) 'sold',\n" +
            "      sum(case status when 1 then 1 else 0 end) 'discontinued'\n" +
            "from dish;")
    SetmealOverViewVO countDishesIsEnableOrNotEnable();


}
