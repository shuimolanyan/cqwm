package com.sky.mapper;

import com.sky.dto.OrdersPageQueryDTO;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdminOrderMapper {
    //分页条件查询
    List<OrderVO> list(OrdersPageQueryDTO ordersPageQueryDTO);

    //统计各个状态的订单的数量
    OrderStatisticsVO countOrderNumberWithStatus();


}
