package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {
    //根据商品或套餐id从购物车中查询商品数据-
    List<ShoppingCart> list(ShoppingCart shoppingCart);

    //新增商品到购物车中
    @Insert("insert into shopping_cart (name, image, user_id, dish_id, setmeal_id, dish_flavor, amount, create_time) values " +
            "(#{name},#{image},#{userId},#{dishId},#{setmealId},#{dishFlavor},#{amount},#{createTime})")
    void insert(ShoppingCart shoppingCart);

    //修改菜品或套餐信息
    void update(ShoppingCart shop);

    //删除商品
    void delete(ShoppingCart shoppingCart);

    //批量添加
    void insertMultiple(List<ShoppingCart> shoppingCarts);

}
