package com.sky.mapper;

import com.sky.entity.AddressBook;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;

@Mapper
public interface AddressBookMapper {
    //新增地址
    @Insert("insert into address_book (user_id, consignee, sex, phone, province_code, province_name, city_code, city_name, district_code, district_name, detail, label, create_time) VALUES " +
            "(#{userId},#{consignee},#{sex},#{phone},#{provinceCode},#{provinceName},#{cityCode},#{cityName},#{districtCode},#{districtName},#{detail},#{label},#{createTime})  ")
    void insert(AddressBook addressBook);

    //查询当前用户下的地址
    List<AddressBook> list(AddressBook addressBook);

    //修改地址信息
    void update(AddressBook addressBook);

    //根据id删除地址
    @Delete("delete from address_book where id = #{id}")
    void delete(AddressBook addressBook);
}
