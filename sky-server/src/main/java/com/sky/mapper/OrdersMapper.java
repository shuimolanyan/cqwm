package com.sky.mapper;

import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.OrderVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface OrdersMapper {
    //新增
    void insert(Orders orders);

    //分页查询
    List<OrderVO> page(Orders orders);

    //查询订单
    OrderVO list(Orders orders);

    //修改订单信息
    void update(Orders orders);

    @Select("select * from orders where status = #{status} and order_time < #{beforeTime}")
    List<Orders> findOrdersByTime(Integer status, LocalDateTime beforeTime);

    //根据状态查询订单
    @Select("select\n" +
            "    sum(if(status = 2,1,0)) 'waitingOrders',\n"+
            "    sum(if(status = 3,1,0)) 'deliveredOrders',\n" +
            "    sum(if(status = 5,1,0)) 'completedOrders',\n" +
            "    sum(if(status = 6,1,0)) 'cancelledOrders',\n" +
            "    count(*) 'allOrders'\n" +
            "from orders;\n")
    OrderOverViewVO countOrderByStatus(LocalDateTime begin, LocalDateTime end, Object status);

}
