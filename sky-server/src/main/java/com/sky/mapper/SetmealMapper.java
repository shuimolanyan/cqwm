package com.sky.mapper;

import com.sky.Aspect.AutoFile;
import com.sky.dto.SetmealDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealOverViewVO;
import com.sky.vo.SetmealVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealMapper {


    //根据分类id查询套餐分类
    @Select("select * from setmeal where category_id = #{id}")
    List<Setmeal> findSetmealByCategoryId(Long id);


    //根据套餐id修改套餐信息
    @AutoFile(OperationType.UPDATE)
    void updateSetmeal(Setmeal setmeal);

    //添加套餐基本字段
    @AutoFile(OperationType.INSERT)
    @Options(useGeneratedKeys = true,keyProperty = "id")
    @Insert("insert into setmeal (name, category_id, price, status, description, image, create_time, update_time, create_user, update_user) VALUES " +
            "(#{name},#{categoryId},#{price},#{status},#{description},#{image},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void addSetmeal(Setmeal setmeal);

    //分页条件查询
    List<SetmealVO> list(Setmeal setmeal);

    //根据ids查询套餐
    Long countSetmealEnableByIds(List<Long> ids);

    //批量伤处
    void deleteSetmealByIds(List<Long> ids);

    //根据id查询套餐
    @Select("select * from setmeal where id = #{id}")
    Setmeal findSetmealById(Setmeal setmeal);

    @Select("select\n" +
            "      sum(case status when 0 then 1 else 0 end) 'sold',\n" +
            "      sum(case status when 1 then 1 else 0 end) 'discontinued'\n" +
            "from setmeal;")
    SetmealOverViewVO countsetmealIsEnableOrNotEnable();

}
