package com.sky.mapper;

import com.sky.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;

@Mapper
public interface UserMapper {
    //分级openid查询用户
    @Select("select id, openid, name, phone, sex, id_number, avatar, create_time from user where  openid = #{openid}")
    User selectUserByOpenId(String openid);


    //新增用户
    @Options(useGeneratedKeys = true,keyProperty = "id")
    @Insert("insert into user (openid, name, phone, sex, id_number, avatar, create_time)" +
            "values (#{openid},#{name},#{phone},#{sex},#{idNumber},#{avatar},#{createTime})")
    void insert(User user);

    //求用户在某个时间之前的用户总量
    @Select("select count(*) from user where create_time < #{beginTime}")
    Integer countBefore(LocalDateTime beginTime);
}

