package com.sky.mapper;

import com.sky.Aspect.AutoFile;
import com.sky.entity.DishFlavor;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishFlavorMapper {
    //添加菜品口味
    void insertMultiple(List<DishFlavor> flavors);

    //根据菜品id删除口味
    void deleteFlavorByDishIds(List<Long> ids);

    //根据菜品id查询口味
    @Select("select id, dish_id, name, value from dish_flavor where dish_id = #{id} ")
    List<DishFlavor> findDishFlavorByDishId(Long id);
}
