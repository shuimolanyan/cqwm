package com.sky.webSocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
@ServerEndpoint("/ws/{sid}")
public class WebSocketServer {
    private static Map<String,Session> sessionMap = new HashMap<>();

    @OnOpen
    public void onOpen(Session session,@PathParam("sid") String sid){
        log.info("链接建立：{}",sid);
        sessionMap.put(sid,session);
    }

    @OnMessage
    public void onMessage(Session session,String message){
        log.info("接收到消息：{}",message);
    }

    @OnError
    public void onError(Session session,Throwable throwable){
        log.info("链接出错：");
        throwable.printStackTrace();
    }

    @OnClose
    public void onClose(Session session,@PathParam("sid") String sid){
        log.info("链接关闭");
        sessionMap.remove("sid");
    }

    public void sendMsg(String msg) throws IOException {
        Collection<Session> sessions = sessionMap.values();
        if(!CollectionUtils.isEmpty(sessions)){
            for (Session session : sessions) {
                session.getBasicRemote().sendText(msg);
            }
        }

    }

}
