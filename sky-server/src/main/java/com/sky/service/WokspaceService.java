package com.sky.service;

import com.sky.vo.BusinessDataVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;

public interface WokspaceService {
    //查询今日营业数据
    BusinessDataVO findBusinessData();

    //查询今日订单数据
    OrderOverViewVO findOrderDataToday();

    //查询菜品总览
    SetmealOverViewVO findOverDisherView();

    //查询套餐总览
    SetmealOverViewVO findOverSetmealView();

}
