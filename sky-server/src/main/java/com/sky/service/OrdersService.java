package com.sky.service;

import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersSubmitDTO;
import com.sky.result.PageResult;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;

import java.io.IOException;

public interface OrdersService {
    //提交订单/下单
    OrderSubmitVO submitOrders(OrdersSubmitDTO ordersSubmitDTO) throws IOException;

    //查找历史订单
    PageResult findHistoryOrders(OrdersPageQueryDTO ordersPageQueryDTO);

    //查询订单详情
    OrderVO findOrderDeatilById(Long id);

    //取消订单
    void cancelOrder(Long id);

    //再次购买
    void onceBy(Long id);

    //催单
    void reminderOrder(Long id) throws IOException;
}
