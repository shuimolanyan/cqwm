package com.sky.service;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;

import java.util.List;

public interface ShoppingCartService {
    //添加商品到购物车中
    void addShopToCart(ShoppingCartDTO shoppingCartDTO);

    //查询购物车中的商品
    List<ShoppingCart> findShopOnCart();

    //从购物车中删除商品
    void subShopFromCart(ShoppingCartDTO shoppingCartDTO);

    //清空购物车
    void clean();

}
