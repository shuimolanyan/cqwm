package com.sky.service;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import java.util.List;

public interface SetmealService {



    //新增套餐
    void save(SetmealDTO setmealDTO);

    //分页条件查询
    PageResult page(SetmealPageQueryDTO setmealPageQueryDTO);

    //删除套餐
    void deleteSetmeal(List<Long> ids);

    //根据id查询套餐
    SetmealVO findSetmealById(Long id);

    //修改套餐
    void update(SetmealDTO setmealDTO);

    //套餐的起售/停售
    void updateSetmealStatus(Integer status, Long id);

    //根据套餐id查询菜品
    List<DishItemVO> findDishFromSetmealById(Long id);
}
