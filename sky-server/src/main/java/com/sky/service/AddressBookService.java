package com.sky.service;

import com.sky.dto.AddressBookDTO;
import com.sky.entity.AddressBook;

import java.util.List;

public interface AddressBookService {
    //新增地址
    void addAddress(AddressBookDTO addressBookDTO);

    //查询当前用户的地址
    List<AddressBook> list();

    //将某个地址设为默认值
    void updateAddressIsDefault(AddressBookDTO addressBookDTO);

    //查询默认地址
    List<AddressBook> findDefaultAddress();

    //根据id查询地址
    AddressBook findAddressById(Long id);

    //根据Id修改地址信息
    void updateAddressById(AddressBookDTO addressBookDTO);

    //根据id删除地址
    void deleteAddress(Long id);
}
