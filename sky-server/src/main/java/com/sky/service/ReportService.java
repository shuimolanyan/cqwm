package com.sky.service;

import com.sky.vo.OrderReportVO;
import com.sky.vo.SalesTop10ReportVO;
import com.sky.vo.TurnoverReportVO;
import com.sky.vo.UserReportVO;

import java.io.IOException;
import java.time.LocalDate;

public interface ReportService {
    //营业额统计
    TurnoverReportVO reportMoney(LocalDate begin, LocalDate end);

    //用户数据统计
    UserReportVO reoportUserData(LocalDate begin, LocalDate end);

    //订单数据统计
    OrderReportVO reoportOrderData(LocalDate begin, LocalDate end);

    //统计排名前十的菜品
    SalesTop10ReportVO top10Report(LocalDate begin, LocalDate end);

    //下载为Excel表格
    void exportExcel() throws Exception;

}
