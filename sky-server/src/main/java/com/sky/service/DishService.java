package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;

import java.util.List;

public interface DishService {
    //新增菜品
    void save(DishDTO dishDTO);

    //分页条件查询
    PageResult page(DishPageQueryDTO dishPageQueryDTO);

    //根据ids批量删除
    void deleteDishByIds(List<Long> ids);

    //根据id查询菜品
    DishVO findDishById(Long id);

    //修改菜品信息
    void updateDish(DishDTO dishDTO);

    //修改菜品的状态（0起售 1 禁售）
    void updateStatus(Integer status, Long id);

    //根据分类id查询菜品信息
    List findDishByCategoryIdOrName(DishDTO dishDTO);

    //根据分类id查询菜品
    List<DishVO> findDishByCategoryId(Long categoryId);
}
