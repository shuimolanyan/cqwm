package com.sky.service.impl;

import com.sky.constant.RedisConstant;
import com.sky.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class ShopServiceImpl implements ShopService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Integer getShopStatus() {
        Object status = redisTemplate.opsForValue().get(RedisConstant.SHOP_STATUS_KEY);

        return (Integer) status;
    }

    @Override
    public void setStatus(Integer status) {
        redisTemplate.opsForValue().set(RedisConstant.SHOP_STATUS_KEY,status,60*60*8, TimeUnit.SECONDS);
    }
}
