package com.sky.service.impl;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.SalesReportDTO;
import com.sky.dto.TurnoverReportDTO;
import com.sky.dto.UserReportDTO;
import com.sky.entity.Orders;
import com.sky.mapper.ReportMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportService;
import com.sky.vo.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportMapper reportMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private HttpServletResponse response;
    /**
     * 营业额统计
     */
    @Override
    public TurnoverReportVO reportMoney(LocalDate begin, LocalDate end) {
        //1.得到要统计的时间段
        List<String> dataList = getDataList(begin, end);

        //2.查询该时间断内的数据
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        List<TurnoverReportDTO> turnoverReportDtoList = reportMapper.selectDataInTime(beginTime,endTime, Orders.ORDER_STAUTS_COMPLETED);

        //2.1获取查询到数据，并将其转换为map k-日期 v-数据
        Map<String, BigDecimal> dataMap = turnoverReportDtoList.stream()
                .collect(Collectors.toMap(TurnoverReportDTO::getOrderDate, TurnoverReportDTO::getOrderMoney));

        //2.2要统计dataList内全部的日期数据（没有数据则为0），但dataMap只查到了有数据的日期，要对其进行处理
        List<BigDecimal> turnoverList = dataList.stream()
                .map(data -> dataMap.get(data) == null ? new BigDecimal("0") : dataMap.get(data))
                .collect(Collectors.toList());

        //3.封装数据并返回
        return new TurnoverReportVO(dataList,turnoverList);
    }

    /**
     * 用户统计报表
     */
    @Override
    public UserReportVO reoportUserData(LocalDate begin, LocalDate end) {
        //1.得到要统计的时间段
        List<String> dataList = getDataList(begin, end);

        //2.得到时间段内的用户数据
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        List<UserReportDTO> userReportDTOList = reportMapper.selectUserDataInTime(beginTime,endTime);

        //2.1将得到的list数据转为map
        Map<String, Integer> dataMap = userReportDTOList.stream().collect(Collectors.toMap(UserReportDTO::getCreateDate, UserReportDTO::getUserCount));

        //2.2为dataList里面的所有日期赋值，得到每天的用户数
        List<Integer> newUserList = dataList.stream().map(data -> {
            return dataMap.get(data) == null ? 0 : dataMap.get(data);
        }).collect(Collectors.toList());

        //3.求用户在这段时间段内的增量
        ArrayList<Integer> totalUserList = new ArrayList<>();

        //3.1求用户在参数中开始时间之前的用户注册数量
        Integer count = userMapper.countBefore(beginTime);
        for (Integer userCountOneDay : newUserList) {
            count += userCountOneDay;
            totalUserList.add(count);
        }
        //4.组装数据并返回
        return new UserReportVO(dataList,totalUserList,newUserList);
    }

    /**
     * 订单数据统计
     */
    @Override
    public OrderReportVO reoportOrderData(LocalDate begin, LocalDate end) {
        //1.得到时间段
        List<String> dataList = getDataList(begin, end);

        //2.得到时间段内每日的订单数据
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        List<OrderReportDTO> orderCounts = reportMapper.selectOrderDataInTime(beginTime,endTime,null);

        //2.1将集合数据转为map
        Map<String, Integer> orderCountMap = orderCounts.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));

        //2.2为dataList里的所有日期赋值
        List<Integer> orderCountList = dataList.stream().map(data -> orderCountMap.get(data) == null ? 0 : orderCountMap.get(data)).collect(Collectors.toList());

        //3.得到时间段内每日的有效订单数据
        List<OrderReportDTO> validOrderCounts = reportMapper.selectOrderDataInTime(beginTime,endTime,5);

        //3.1将集合数据转为map
        Map<String, Integer> validOrderCountMap = validOrderCounts.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));

        //3.2为dataList里的所有日期赋值
        List<Integer> validOrderCountList = dataList.stream().map(data -> validOrderCountMap.get(data) == null ? 0 : validOrderCountMap.get(data)).collect(Collectors.toList());

        //4.计算订单总数
        Integer totalOrderCount = orderCountList.stream().reduce(Integer::sum).get();

        //5.计算有效订单数
        Integer validOrderCount = validOrderCountList.stream().reduce(Integer::sum).get();

        //6.计算订单完成率
        Double orderCompletionRate = validOrderCount.doubleValue() / totalOrderCount;

        //7.处理数据并返回
        return new OrderReportVO(dataList,orderCountList,validOrderCountList,totalOrderCount,validOrderCount,orderCompletionRate);

    }

    /**
     * 统计排名前十的菜品
     */
    @Override
    public SalesTop10ReportVO top10Report(LocalDate begin, LocalDate end) {
        //1.查询在时间段内的菜品排名数据
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        List<SalesReportDTO> salesReportDTOList = reportMapper.selectTop10DishDataInTime(beginTime,endTime,Orders.ORDER_STAUTS_COMPLETED);

        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<Integer> numberList = new ArrayList<>();

        for (SalesReportDTO salesReportDTO : salesReportDTOList) {
            nameList.add(salesReportDTO.getGoodsName());
            numberList.add(salesReportDTO.getGoodsNumber());
        }
        return new SalesTop10ReportVO(nameList,numberList);
    }

    private List<String> getDataList(LocalDate begin, LocalDate end) {

        Stream<LocalDate> dateStream = begin.datesUntil(end.plusDays(1));

        return dateStream.map(localDate -> {
            return localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }).collect(Collectors.toList());

    }

    /**
     * 将统计的数据下载为表格
     */
    @Override
    public void exportExcel() throws Exception {
        //1.得到时间段
        LocalDate begin = LocalDate.now().minusDays(30);
        LocalDate end = LocalDate.now().minusDays(1);

        String TimeInterval ="数据统计时间:" + begin.toString()+"~"+end.toString();

        //2.得到数据
        BusinessDataVO businessDataVO = reportMapper.selectData(LocalDateTime.of(begin,LocalTime.MIN),LocalDateTime.of(end,LocalTime.MAX));
        Integer count = reportMapper.countUser(LocalDateTime.of(begin,LocalTime.MIN),LocalDateTime.of(end,LocalTime.MAX));

        //3.加载模板文件
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("templates/运营数据报表模板.xlsx");
        Workbook workbook = new XSSFWorkbook(in);

        //4.填充
        Sheet sheet = workbook.getSheetAt(0);

        //填充时间
        Row row1 = sheet.getRow(1);
        row1.getCell(1).setCellValue(TimeInterval);

        //填充橄榄数据
        Row row3 = sheet.getRow(3);
        row3.getCell(2).setCellValue(businessDataVO.getTurnover());
        row3.getCell(4).setCellValue(businessDataVO.getOrderCompletionRate());
        row3.getCell(6).setCellValue(count);
        Row row4 = sheet.getRow(4);
        row4.getCell(2).setCellValue(businessDataVO.getValidOrderCount());
        row4.getCell(4).setCellValue(businessDataVO.getUnitPrice());

        //填充明细数据
        for (int i = 0; i < 30; i++) {
            LocalDate _begin = begin.plusDays(i);
            BusinessDataVO _businessDataVO = reportMapper.selectData(LocalDateTime.of(_begin, LocalTime.MIN), LocalDateTime.of(_begin, LocalTime.MAX));
            Integer _count = reportMapper.countUser(LocalDateTime.of(_begin, LocalTime.MIN), LocalDateTime.of(_begin, LocalTime.MAX));
            Row row = sheet.getRow(7 + i);
            row.getCell(1).setCellValue(_begin.toString());
            row.getCell(2).setCellValue(_businessDataVO.getTurnover());
            row.getCell(3).setCellValue(_businessDataVO.getValidOrderCount());
            row.getCell(4).setCellValue(_businessDataVO.getOrderCompletionRate());
            row.getCell(5).setCellValue(_businessDataVO.getUnitPrice());
            row.getCell(6).setCellValue(_count);
        }

        //5.以流的形式响应给前端，下载表格
        ServletOutputStream out = response.getOutputStream();
        workbook.write(out);

        //6.关闭资源
        workbook.close();
        in.close();
    }
}
