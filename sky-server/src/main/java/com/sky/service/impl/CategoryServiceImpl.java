package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.context.BaseContext;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.exception.BusinessException;
import com.sky.mapper.CategoryMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.CategoryService;
import com.sky.utils.BeanHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.sky.constant.MessageConstant.CATEGORY_BE_RELATED_BY_DISH;
import static com.sky.constant.MessageConstant.CATEGORY_BE_RELATED_BY_SETMEAL;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private SetmealMapper setmealMapper;
    /**
     * 分页查询
     * @param categoryPageQueryDTO
     * @return
     */
    @Override
    public PageResult page(CategoryPageQueryDTO categoryPageQueryDTO) {
        PageHelper.startPage(categoryPageQueryDTO.getPage(),categoryPageQueryDTO.getPageSize());

        Category category = BeanHelper.copyProperties(categoryPageQueryDTO, Category.class);
        List<Category> categoryList = categoryMapper.list(category);

        Page p = (Page) categoryList;

        return new PageResult(p.getTotal(),p.getResult());

    }

    /**
     *  根据id查询分类
     * @param id
     */
    @Override
    public Category findCategoryById(Long id) {
        Category category = Category.builder().id(id).build();
        List<Category> categoryList =  categoryMapper.list(category);
        return categoryList.get(0);
    }

    /**
     * 修改分类
     * @param categoryDTO
     */
    @Override
    public void updateCategory(CategoryDTO categoryDTO) {
        Category category = BeanHelper.copyProperties(categoryDTO, Category.class);
        category.setUpdateTime(LocalDateTime.now());
        category.setUpdateUser(BaseContext.getCurrentId());
        categoryMapper.update(category);
    }

    /**
     * 禁用/启用
     * @param status
     * @param id
     */
    @Override
    public void updateStatus(Integer status, Long id) {
        Category category = Category.builder().id(id).status(status).build();
//        category.setUpdateTime(LocalDateTime.now());
//        category.setUpdateUser(BaseContext.getCurrentId());
        categoryMapper.update(category);
    }

    /**
     * 新增分类
     * @param categoryDTO
     */
    @Override
    public void insetCategory(CategoryDTO categoryDTO) {
        Category category = BeanHelper.copyProperties(categoryDTO, Category.class);
//        category.setCreateTime(LocalDateTime.now());
//        category.setCreateUser(BaseContext.getCurrentId());
//        category.setUpdateTime(LocalDateTime.now());
//        category.setUpdateUser(BaseContext.getCurrentId());
        categoryMapper.insert(category);
    }

    /**
     * 删除分类
     * @param id
     */
    @Override
    public void deleteCategory(Long id) {
        List<Dish> dishList = dishMapper.findDishByCateoryId(id);
        if(CollectionUtils.isEmpty(dishList)){
            throw new BusinessException(CATEGORY_BE_RELATED_BY_DISH);
        }

        List<Setmeal> setmealList = setmealMapper.findSetmealByCategoryId(id);
        if(CollectionUtils.isEmpty(setmealList)){
            throw new BusinessException(CATEGORY_BE_RELATED_BY_SETMEAL);
        }
        categoryMapper.delete(id);
    }

    /**
     * 根据类型查询分类
     * @param categoryDTO
     * @return
     */
    @Override
    public List<Category> findCategoryByType(CategoryDTO categoryDTO) {
        Category category = BeanHelper.copyProperties(categoryDTO, Category.class);
        List<Category> categoryList = categoryMapper.list(category);
        return categoryList;
    }

    /**
     * C端的根据类型查询分类
     * @param type
     * @return
     */
    @Override
    public List<Category> findCategoryByTypeOnC(Integer type) {
       List<Category> categoryList =  categoryMapper.findCategoryByTypeOnC(type);
       return categoryList;

    }

    /**
     * 根据分类id查询套餐
     * @param id
     * @return
     */
    @Override
    @Cacheable(cacheNames = "setmeal:cache",key = "#a0")
    public List<Setmeal> findSetmealByCategoryId(Long id) {
        List<Setmeal> setmealList = categoryMapper.findSetmealByCategoryId(id);
        return setmealList;
    }
}
