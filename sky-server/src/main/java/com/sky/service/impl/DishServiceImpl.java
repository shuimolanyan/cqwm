package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.RedisConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.entity.Setmeal;
import com.sky.exception.BusinessException;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetMealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DishServiceImpl implements DishService {
    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private DishFlavorMapper dishFlavorMapper;

    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private SetMealDishMapper setMealDishMapper;

    @Autowired
    private RedisTemplate<Object,Object> redisTemplate;
    //新增菜品
    @Transactional
    @Override
    public void save(DishDTO dishDTO) {
        //1.先在菜品表中新增，因为口味表要跟随菜品表，需要口味表的dishId与菜品表对应，需要使用主键返回
        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);
        if(dish != null){
            dish.setStatus(StatusConstant.DISABLE);
            dishMapper.insert(dish);
            //2.保存口味
            List<DishFlavor> flavors = dishDTO.getFlavors();
            if(!CollectionUtils.isEmpty(flavors)){
                flavors.forEach(flavor -> {
                    flavor.setDishId(dish.getId());
                });

                dishFlavorMapper.insertMultiple(flavors);
            }
        }
    }

    /**
     * 条件分页查询
     * @param dishPageQueryDTO
     * @return
     */
    @Override
    public PageResult page(DishPageQueryDTO dishPageQueryDTO) {
        PageHelper.startPage(dishPageQueryDTO.getPage(),dishPageQueryDTO.getPageSize());

        List<DishVO> dishVOList = dishMapper.list(dishPageQueryDTO);

        Page<DishVO> page = (Page<DishVO>) dishVOList;

        return new PageResult(page.getTotal(),page.getResult());
    }

    /**
     * 根据ids批量删除
     * @param ids
     */
    @Transactional
    @Override
    public void deleteDishByIds(List<Long> ids) {
        //1.如果状态为起售不能删除
        Long count = dishMapper.countEnableDishByIds(ids);
        if(count > 0 ){
            throw new BusinessException(MessageConstant.DISH_ON_SALE);
        }
        //2.如果包含在套餐表中不能删
        List<Long> setmealIds = setMealDishMapper.countDishInSetmeal(ids);
        if(!CollectionUtils.isEmpty(setmealIds)){
            throw new BusinessException(MessageConstant.CATEGORY_BE_RELATED_BY_SETMEAL);
        }
        //3.菜品删除
        dishMapper.deleteDishByIds(ids);

        //4.口味表也要跟着删除
        dishFlavorMapper.deleteFlavorByDishIds(ids);
    }

    /**
     * 根据id查询数据
     * @param id
     * @return
     */
    @Transactional
    @Override
    public DishVO findDishById(Long id) {
        Dish dish = dishMapper.findDishById(id);

        List<DishFlavor> dishFlavorList = dishFlavorMapper.findDishFlavorByDishId(id);

        DishVO dishVO = BeanHelper.copyProperties(dish, DishVO.class);
        if(dishVO != null){
            dishVO.setFlavors(dishFlavorList);
        }

        return dishVO;
    }

    /**
     * 修改菜品信息
     * @param dishDTO
     */
    @Transactional
    @Override
    public void updateDish(DishDTO dishDTO) {

        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);
        //1.要修改基本信息
        dishMapper.updateDish(dish);

        //2.要修改口味信息
        //2.1直接吧菜品对应的口味表中的口味全部删除
        dishFlavorMapper.deleteFlavorByDishIds(Collections.singletonList(dishDTO.getId()));

        //2.2从新添加
        List<DishFlavor> flavors = dishDTO.getFlavors();
        if(!CollectionUtils.isEmpty(flavors)){
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dishDTO.getId());
            });
            dishFlavorMapper.insertMultiple(flavors);
        }
    }

    /**
     * 修改菜品的状态
     * @param status
     * @param id
     */
    @Transactional
    @Override
    public void updateStatus(Integer status, Long id) {
        //1.起售
        //1.1菜品起售
        Dish dish = Dish.builder().id(id).status(status).build();
        if(StatusConstant.ENABLE.equals(status) ){
            dishMapper.updateDish(dish);
        }

        //2.停售
        //2.1菜品停售
        dishMapper.updateDish(dish);
        //2.2菜品停售相关联的套餐也要停售
        List<Long> setmealIds = setMealDishMapper.countDishInSetmeal(Collections.singletonList(id));

        if(!CollectionUtils.isEmpty(setmealIds)){
            setmealIds.forEach(setmealId -> {
                Setmeal setmeal = Setmeal.builder().id(setmealId).status(status).build();
                setmealMapper.updateSetmeal(setmeal);
            });

        }
    }

    @Override
    public List findDishByCategoryIdOrName(DishDTO dishDTO) {

        List<Dish> dishList = dishMapper.findDishByCategoryIdOrName(dishDTO);
        return dishList;
    }

    /**
     * 根据分类id查询菜品及其口味
     * @param categoryId
     * @return
     */
    @Override
    @Transactional
    public List<DishVO> findDishByCategoryId(Long categoryId) {
        //1.从redis中查询数据，如果有则返回，没有则去数据库中查询数据
        List<DishVO> dishVOList = (List<DishVO>) redisTemplate.opsForValue().get(RedisConstant.CACHE_DISH_KEY + categoryId);
        if(!CollectionUtils.isEmpty(dishVOList)){
            return dishVOList;
        }

        //2.在数据库中查询数据
        List<Dish> dishList = dishMapper.findDishByCateoryId(categoryId);

        List<DishVO> dishVOS = dishList.stream().map(dish -> {
            List<DishFlavor> dishFlavors = dishFlavorMapper.findDishFlavorByDishId(dish.getId());
            DishVO dishVO = BeanHelper.copyProperties(dish, DishVO.class);
            dishVO.setFlavors(dishFlavors);
            return dishVO;
        }).collect(Collectors.toList());



        //3.将数据库中查询到的数据缓存到redis中
        log.info("缓存到Redis中");
        redisTemplate.opsForValue().set(RedisConstant.CACHE_DISH_KEY+categoryId,dishVOS);

        return dishVOS;
    }
}
