package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.OrdersCancelDTO;
import com.sky.dto.OrdersConfirmDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersRejectionDTO;
import com.sky.entity.OrderDetail;
import com.sky.entity.Orders;
import com.sky.exception.BusinessException;
import com.sky.mapper.AdminOrderMapper;
import com.sky.mapper.OrdersMapper;
import com.sky.result.PageResult;
import com.sky.service.AdminOrderService;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AdminOrderServiceImpl implements AdminOrderService {
    @Autowired
    private AdminOrderMapper adminOrderMapper;

    @Autowired
    private OrdersMapper ordersMapper;

    /**
     * 条件分页查询
     *
     * @param ordersPageQueryDTO
     * @return
     */
    @Override
    public PageResult list(OrdersPageQueryDTO ordersPageQueryDTO) {
        PageHelper.startPage(ordersPageQueryDTO.getPage(), ordersPageQueryDTO.getPageSize());

        List<OrderVO> orderVOS = adminOrderMapper.list(ordersPageQueryDTO);
        getOrderDishs(orderVOS);

        Page<OrderVO> p = (Page<OrderVO>) orderVOS;

        return new PageResult(p.getTotal(), p.getResult());

    }

    private void getOrderDishs(List<OrderVO> orderVOS) {
        for (OrderVO orderVO : orderVOS) {
            List<OrderDetail> orderDetailList = orderVO.getOrderDetailList();
            StringBuilder orderDishs = null;
            for (OrderDetail orderDetail : orderDetailList) {
                orderDishs = new StringBuilder();
                orderDishs.append(orderDetail.getName());
            }
            orderVO.setOrderDishes(orderDishs.toString());
        }
    }

    /**
     * 统计各个状态的订单的数量
     *
     * @return
     */
    @Override
    public OrderStatisticsVO countOderStatusNumber() {
        OrderStatisticsVO orderStatisticsVO = adminOrderMapper.countOrderNumberWithStatus();
        return orderStatisticsVO;
    }

    /**
     * 查询订单详情
     */
    @Override
    public OrderVO findOrderDetailById(Long id) {
        Orders orders = Orders.builder().id(id).userId(BaseContext.getCurrentId()).build();
        OrderVO orderVO = ordersMapper.list(orders);
        return orderVO;
    }

    /**
     * 接单
     *
     * @param ordersConfirmDTO
     */
    @Override
    @Transactional
    public void receiveOrder(OrdersConfirmDTO ordersConfirmDTO) {
        Orders orders = Orders.builder()
                .id(ordersConfirmDTO.getId())
                .userId(BaseContext.getCurrentId())
                .build();
        OrderVO orderVO = ordersMapper.list(orders);

        if (orderVO.getStatus() == Orders.ORDER_STAUTS_TO_BE_CONFIRMED) {
            orders = Orders.builder()
                    .id(ordersConfirmDTO.getId())
                    .userId(BaseContext.getCurrentId())
                    .status(Orders.ORDER_STAUTS_CONFIRMED)
                    .build();
            ordersMapper.update(orders);
        }


    }

    /**
     * 拒单
     *
     * @param ordersRejectionDTO
     */
    @Override
    @Transactional
    public void rejectOrder(OrdersRejectionDTO ordersRejectionDTO) {
        Orders orders = Orders.builder().id(ordersRejectionDTO.getId())
                .userId(BaseContext.getCurrentId())
                .build();
        OrderVO orderVO = ordersMapper.list(orders);


        orders = Orders.builder()
                .id(ordersRejectionDTO.getId())
                .userId(BaseContext.getCurrentId())
                .rejectionReason(ordersRejectionDTO.getRejectionReason())
                .build();
        //2.判断是否支付,已付款
        if(orderVO.getPayStatus() == Orders.ORDER_STAUTS_PENDING_PAYMENT){
            orders.setPayStatus(Orders.PAY_STATUS_REFUND);
            //退款操作
        }
        //未付款，只将状态改为取消
        orders.setStatus(Orders.ORDER_STAUTS_CANCELLED);
        ordersMapper.update(orders);

    }

    /**
     * 取消订单
     * @param ordersCancelDTO
     */
    @Override
    @Transactional
    public void cancelOrder(OrdersCancelDTO ordersCancelDTO) {
        Orders orders = Orders.builder()
                .id(ordersCancelDTO.getId())
                .userId(BaseContext.getCurrentId())
                .cancelReason(ordersCancelDTO.getCancelReason())
                .build();
        //1.查询订单是否存在
        OrderVO orderVO = ordersMapper.list(orders);

        if(orderVO == null){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }

        orders = Orders.builder()
                .id(ordersCancelDTO.getId())
                .userId(BaseContext.getCurrentId())
                .rejectionReason(ordersCancelDTO.getCancelReason())
                .build();

        //2.判断是否支付,若已支付还需退款
        if(orderVO.getPayStatus() == Orders.PAY_STATUS_PAID){
            orders.setPayStatus(Orders.PAY_STATUS_REFUND);
            //退款操作
        }
        //未付款，只将状态改为取消
        orders.setStatus(Orders.ORDER_STAUTS_CANCELLED);
        ordersMapper.update(orders);
    }

    /**
     * 派送订单
     */
    @Override
    public void deliveryOrder(Long id) {
        //1.判断订单是否存在 并且只有已接单的订单才能派送
        Orders orders = Orders.builder()
                .id(id)
                .userId(BaseContext.getCurrentId())
                .build();
        OrderVO orderVO = ordersMapper.list(orders);

        if(orderVO == null){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }

        //只有已接单的订单才能派送
        if(orderVO.getStatus() == Orders.ORDER_STAUTS_CONFIRMED){
            //2.修改订单状态为派送中
            orders.setStatus(Orders.ORDER_STAUTS_DELIVERY_IN_PROGRESS);
            ordersMapper.update(orders);
        }

    }

    /**
     * 完成订单
     * @param id
     */
    @Override
    public void susccessOrder(Long id) {
        //1.判断订单是否存在 并且只有派送中单的订单才能完成
        Orders orders = Orders.builder()
                .id(id)
                .userId(BaseContext.getCurrentId())
                .build();
        OrderVO orderVO = ordersMapper.list(orders);

        if(orderVO == null){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }

        //只有已派送中的订单才能完成
        if(orderVO.getStatus() == Orders.ORDER_STAUTS_DELIVERY_IN_PROGRESS){
            //2.修改订单状态为派送中
            orders.setStatus(Orders.ORDER_STAUTS_COMPLETED);
            ordersMapper.update(orders);
        }
    }
}
