package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.SetmealDTO;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.service.ShoppingCartService;
import com.sky.utils.BeanHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.CastUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ShoppingCartIServiceImpl implements ShoppingCartService {
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private SetmealMapper setmealMapper;

    /**
     * 添加商品到购物车中
     *
     * @param shoppingCartDTO
     */
    @Override
    public void addShopToCart(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCart shoppingCart = ShoppingCart.builder().dishFlavor(shoppingCartDTO.getDishFlavor())
                .dishId(shoppingCartDTO.getDishId())
                .setmealId(shoppingCartDTO.getSetmealId())
                .userId(BaseContext.getCurrentId()).build();

        //1.查询购物车，查看该商品是否已经存在
        //根据提供的参数就可以定位到唯一的商品了
        List<ShoppingCart> shoppingCarts = shoppingCartMapper.list(shoppingCart);

        if (CollectionUtils.isEmpty(shoppingCarts)) {
            //2.不存在则新增
            if (shoppingCart.getDishId() != null) { //菜品
                Dish dish = dishMapper.findDishById(shoppingCart.getDishId());
                shoppingCart.setAmount(dish.getPrice());
                shoppingCart.setImage(dish.getImage());
                shoppingCart.setName(dish.getName());
            } else { //套餐
                Setmeal setmeal = Setmeal.builder().id(shoppingCart.getSetmealId()).build();
                setmeal = setmealMapper.findSetmealById(setmeal);
                shoppingCart.setAmount(setmeal.getPrice());
                shoppingCart.setName(setmeal.getName());
                shoppingCart.setImage(setmeal.getImage());
            }
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCart.setNumber(1);
            //新增
            shoppingCartMapper.insert(shoppingCart);
        }else {
            //3.存在则数量加一
            ShoppingCart shop = shoppingCarts.get(0);
            shop.setNumber(shop.getNumber()+1);
            //3.1再插入到数据库商品库中
            shoppingCartMapper.update(shop);
        }
    }

    /**
     * 查询购物车中的商品
     * @return
     */
    @Override
    public List<ShoppingCart> findShopOnCart() {
        ShoppingCart shoppingCart = ShoppingCart.builder()
                .userId(BaseContext.getCurrentId())
                .build();
        List<ShoppingCart> shops = shoppingCartMapper.list(shoppingCart);
        return shops;
    }

    /**
     * 从购物车中删除商品
     * @param shoppingCartDTO
     */
    @Override
    public void subShopFromCart(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCart shoppingCart = BeanHelper.copyProperties(shoppingCartDTO, ShoppingCart.class);
        shoppingCart.setUserId(BaseContext.getCurrentId());

        //1.根据套餐或菜品id和口味从数据库中定位到该商品
        List<ShoppingCart> shoppingCarts = shoppingCartMapper.list(shoppingCart);
        if(!CollectionUtils.isEmpty(shoppingCarts)){
            ShoppingCart shop = shoppingCarts.get(0);
            //2.如果商品数量大于1则数量-1
            if(shop.getNumber() > 1){
                shop.setNumber(shop.getNumber()-1);
                //修改数据库中的数据
                shoppingCartMapper.update(shop);
            }else {
                //2.如果数量为1则从数据库中删除该商品
                shoppingCartMapper.delete(shoppingCart);
            }
        }
    }

    /**
     * 清空购物车
     */
    @Override
    public void clean() {
        ShoppingCart shoppingCart = ShoppingCart.builder().build();
        shoppingCartMapper.delete(shoppingCart);
    }
}
