package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.exception.BusinessException;
import com.sky.exception.DataException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import com.sky.utils.BeanHelper;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;

import java.awt.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.sky.constant.MessageConstant.*;
import static com.sky.constant.PasswordConstant.DEFAULT_PASSWORD;
import static com.sky.constant.StatusConstant.ENABLE;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    private static final String LOG_ERROR_COUNT = "log:error:count:";
    private static final String LOG_ERROR_LOCK = "log:error:lock:";
    /**
     * 登录
     *
     * @param employeeLoginDTO
     * @return
     */
    @Override
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();

        //0.判断redis中是否有密码输入错误超过5此的key值
        Object flag = redisTemplate.opsForValue().get(LOG_ERROR_LOCK + username);
        //如果是null则说明账号没有被锁住
        if(!ObjectUtils.isEmpty(flag)){
            throw new BusinessException(LOGIN_ACCOUNT_LOCK);
        }


        //1.根据前端传来的用户名查询员工
        Employee employee = employeeMapper.findEmployeeByUsername(username);
        log.info("根据用户名查询到的用户信息：{}", employee);

        //2.员工数据为null，返回错误信息
        if (employee == null) {
            log.error("员工的数据为null");
            throw new DataException(ACCOUNT_NOT_FOUND);
        }

        //3.校验密码，错误，返回错误信息
        String beforePassword = employeeLoginDTO.getPassword();
        String mysqlPassword = employee.getPassword();
        String TransferPassword = DigestUtils.md5DigestAsHex(beforePassword.getBytes());

        if (!mysqlPassword.equals(TransferPassword)) {
            log.error("密码错误");

            //生成一个5分钟的有效时间key
            redisTemplate.opsForValue().set( LOG_ERROR_COUNT + username + UUID.randomUUID(), 1, 300, TimeUnit.SECONDS);
            //3.1从redis中获取每一失效的key
            Set<Object> keys = redisTemplate.keys("log:error*");
            if (keys != null && keys.size() >= 5) {
                log.error("五分钟内密码多次错误，账号锁定一小时！");
                redisTemplate.opsForValue().set( LOG_ERROR_LOCK + username, 1, 3600, TimeUnit.SECONDS);
                throw new BusinessException(PASSWORD_MULTIPLE_ERROR_LOCK);
            }

            throw new BusinessException(PASSWORD_ERROR);
        }

        //4.校验登录状态，如果为0则返回错误信息
        if (employee.getStatus() == 0) {
            log.error("该用户已被锁定");
            throw new BusinessException(ACCOUNT_LOCKED);
        }

        //5.返回员工信息
        return employee;
    }

    /**
     * 新增员工
     * @param employeeDTO
     */
    @Override
    public void save(EmployeeDTO employeeDTO) {
        //1.将前端传来的参数赋值给员工完整信息，用来加入数据库

        Employee employee = BeanHelper.copyProperties(employeeDTO, Employee.class);

        employee.setPassword(DigestUtils.md5DigestAsHex(DEFAULT_PASSWORD.getBytes()));
        employee.setStatus(ENABLE);

//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());
//
//        employee.setCreateUser(BaseContext.getCurrentId());
//        employee.setUpdateUser(BaseContext.getCurrentId());

        //2.将数据加入数据库中
        employeeMapper.insertEmployee(employee);
    }

    /**
     * 分页查询
     * @param employeePageQueryDTO
     * @return
     */
    @Override
    public PageResult page(EmployeePageQueryDTO employeePageQueryDTO) {
        PageHelper.startPage(employeePageQueryDTO.getPage(),employeePageQueryDTO.getPageSize());

        List<Employee> employeeList = employeeMapper.list(employeePageQueryDTO.getName());

        Page page = (Page) employeeList;
        return new PageResult(page.getTotal(),page.getResult());
    }

    /**
     * 启用禁用
     * @param status
     * @param id
     */
    @Override
    public void enableOrDisable(Integer status, Long id) {
        Employee employee = Employee.builder()
                .id(id)
                .status(status).build();
//                .updateTime(LocalDateTime.now())
//                .updateUser(BaseContext.getCurrentId()).build();

        employeeMapper.update(employee);
    }

    /**
     * 根据id查询员工
     * @param id
     * @return
     */
    @Override
    public Employee findEmployeeById(Integer id) {
        Employee employee = employeeMapper.findEmployeeById(id);
        return employee;
    }

    /**
     * 修改员工信息
     * @param employeeDTO
     */
    @Override
    public void updateEmployee(EmployeeDTO employeeDTO) {
        Employee employee = BeanHelper.copyProperties(employeeDTO, Employee.class);
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser(BaseContext.getCurrentId());
        employeeMapper.update(employee);
    }

    //修改密码
    @Override
    @Transactional
    public void updatePassword(PasswordEditDTO passwordEditDTO) {
        //1.根据id查询到员工的信息
        Employee employeeFromDataBase = employeeMapper.findEmployeeById(Integer.valueOf(BaseContext.getCurrentId().toString()));

        //2.对输入的旧密码进行加密处理
        String oldPassword = passwordEditDTO.getOldPassword();
        String lockOldPassword = DigestUtils.md5DigestAsHex(oldPassword.getBytes());

        //2.1对输入的新密码进行加密处理
        String newPassword = passwordEditDTO.getNewPassword();
        String lockNewPassword = DigestUtils.md5DigestAsHex(newPassword.getBytes());

        //3.检验旧密码是否正确
        String password = employeeFromDataBase.getPassword();
        log.info("前端传入的密码：{}",lockOldPassword);
        log.info("数据库查到的密码：{}",password);
        log.info("修改后的密码：{}",newPassword);
        if(!lockOldPassword.equals(password)){
            throw new BusinessException(OLD_PASSWORD_ERROR);
        }


        //4.将加密的密码存入到修改密码的对象中
        passwordEditDTO.setNewPassword(lockNewPassword);

        log.info("id是{}",BaseContext.getCurrentId());
        //4.修改密码
        Employee employee = Employee.builder().id(BaseContext.getCurrentId()).password(lockNewPassword).build();

        employeeMapper.update(employee);
    }
}
