package com.sky.service.impl;

import com.sky.constant.MessageConstant;
import com.sky.dto.OrderReportDTO;
import com.sky.dto.TurnoverReportDTO;
import com.sky.dto.UserReportDTO;
import com.sky.entity.Orders;
import com.sky.exception.BusinessException;
import com.sky.mapper.DishMapper;
import com.sky.mapper.OrdersMapper;
import com.sky.mapper.ReportMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.service.WokspaceService;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WorkspaceServiceImpl implements WokspaceService {

    @Autowired
    private ReportMapper reportMapper;

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private SetmealMapper setmealMapper;


    private static LocalDateTime begin = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);

    private  static  LocalDateTime end = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
    /**
     * 查询今日营业数据
     * @return
     */
    @Override
    public BusinessDataVO findBusinessData() {
        //新增用户

        List<UserReportDTO> userReportDTOS = reportMapper.selectUserDataInTime(begin, end);
        Integer newUsers = 0;
        if(!CollectionUtils.isEmpty(userReportDTOS)){
            UserReportDTO userReportDTO = userReportDTOS.get(0);
            newUsers = userReportDTO.getUserCount();
        }


        //订单完成率
        //订单总数
        List<OrderReportDTO> orderReportDTOS = reportMapper.selectOrderDataInTime(begin, end, null);
        Integer allOrderCount = 0;
        if(!CollectionUtils.isEmpty(orderReportDTOS)){
            OrderReportDTO allOrderReportDTO = orderReportDTOS.get(0);
            allOrderCount = allOrderReportDTO.getOrderCount();
        }

        //有效订单数
        List<OrderReportDTO> orderReportDTOList = reportMapper.selectOrderDataInTime(begin, end, Orders.ORDER_STAUTS_COMPLETED);
        Integer validOrderCount = 0;
        if(!CollectionUtils.isEmpty(orderReportDTOList)){
            OrderReportDTO orderReportDTO = orderReportDTOList.get(0);
            validOrderCount = orderReportDTO.getOrderCount();
        }


        //完成率
        Double orderCompletionRate = (allOrderCount == 0) ? 0 : validOrderCount.doubleValue() / allOrderCount;

        //营业额
        List<TurnoverReportDTO> turnoverReportDTOS = reportMapper.selectDataInTime(begin, end, Orders.ORDER_STAUTS_COMPLETED);

        Double turnover = 0.0;
        if(!CollectionUtils.isEmpty(turnoverReportDTOS)){
            TurnoverReportDTO turnoverReportDTO = turnoverReportDTOS.get(0);
            turnover = turnoverReportDTO.getOrderMoney().doubleValue();
        }


        //平均客单价
        //查找本日的客源
        Integer countUser = reportMapper.selectOrderDataInTimeByUserId(begin,end,Orders.ORDER_STAUTS_COMPLETED);

        Double unitPrice = (countUser == 0) ? 0.0 : Math.round(turnover)/countUser ;

        return new BusinessDataVO(turnover,validOrderCount,orderCompletionRate,unitPrice,newUsers);

    }

    /**
     * 查询今日订单数据
     * @return
     */
    @Override
    public OrderOverViewVO findOrderDataToday() {
        //全部订单
        OrderOverViewVO orderOverViewVO = ordersMapper.countOrderByStatus(begin,end,null);

        return orderOverViewVO;
    }

    /**
     * 查询菜品总览
     * @return
     */
    @Override
    public SetmealOverViewVO findOverDisherView() {
        SetmealOverViewVO setmealOverViewVO= dishMapper.countDishesIsEnableOrNotEnable();
        System.out.println(setmealOverViewVO);
        return setmealOverViewVO;
    }

    /**
     * 查询套餐总览
     * @return
     */
    @Override
    public SetmealOverViewVO findOverSetmealView() {
        SetmealOverViewVO setmealOverViewVO= setmealMapper.countsetmealIsEnableOrNotEnable();
        return setmealOverViewVO;
    }
}
