package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.AddressBookDTO;
import com.sky.entity.AddressBook;
import com.sky.mapper.AddressBookMapper;
import com.sky.service.AddressBookService;
import com.sky.utils.BeanHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AddressBookBookServiceImpl implements AddressBookService {
    @Autowired
    private AddressBookMapper addressBookMapper;

    /**
     * 新增地址
     * @param addressBookDTO
     */
    @Override
    public void addAddress(AddressBookDTO addressBookDTO) {
        AddressBook addressBook = BeanHelper.copyProperties(addressBookDTO, AddressBook.class);
        addressBook.setUserId(BaseContext.getCurrentId());
        addressBook.setCreateTime(LocalDateTime.now());
        addressBookMapper.insert(addressBook);
    }

    /**
     * 查询当前用户的地址
     * @return
     */
    @Override
    public List<AddressBook> list() {
        AddressBook addressBook = AddressBook.builder().userId(BaseContext.getCurrentId()).build();
        List<AddressBook> addressBooks = addressBookMapper.list(addressBook);
        return addressBooks;
    }

    /**
     * 将指定id的地址设为默认值
     * @param addressBookDTO
     */
    @Override
    public void updateAddressIsDefault(AddressBookDTO addressBookDTO) {

        //1.将所有用户的默认值设置为0
        AddressBook addressBook = AddressBook.builder().isDefault(0).build();
        addressBookMapper.update(addressBook);

        AddressBook isDefaultAddressBook = AddressBook.builder()
                .id(addressBookDTO.getId())
                .userId(BaseContext.getCurrentId())
                .isDefault(1)
                .build();
        addressBookMapper.update(isDefaultAddressBook);
    }

    /**
     * 查询默认地址的用户
     */
    @Override
    public List<AddressBook> findDefaultAddress() {
        AddressBook addressBook = AddressBook.builder()
                .userId(BaseContext.getCurrentId())
                .isDefault(1)
                .build();
        List<AddressBook> addressBooks = addressBookMapper.list(addressBook);
        return addressBooks;
    }

    /**
     * 根据id查询地址
     * @param id
     * @return
     */
    @Override
    public AddressBook findAddressById(Long id) {
        AddressBook addressBook = AddressBook.builder().id(id).build();
        List<AddressBook> addressBooks = addressBookMapper.list(addressBook);

        return addressBooks.get(0);
    }

    /**
     * 根据id修改地址信息
     * @param addressBookDTO
     */
    @Override
    public void updateAddressById(AddressBookDTO addressBookDTO) {
        AddressBook addressBook = BeanHelper.copyProperties(addressBookDTO, AddressBook.class);
        addressBook.setUserId(BaseContext.getCurrentId());
        addressBookMapper.update(addressBook);
    }

    /**
     * 根据id删除地址
     * @param id
     */
    @Override
    public void deleteAddress(Long id) {
        AddressBook addressBook = AddressBook.builder().id(id).build();
        addressBookMapper.delete(addressBook);
    }
}
