package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.BusinessException;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetMealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private SetMealDishMapper setMealDishMapper;

    @Autowired
    private DishMapper dishMapper;
    /**
     * 新增套餐
     * @param setmealDTO
     */
    @Transactional
    @Override
    public void save(SetmealDTO setmealDTO) {
        Setmeal setmeal = BeanHelper.copyProperties(setmealDTO, Setmeal.class);

        //1.新增普通套餐基础字段
        setmealMapper.addSetmeal(setmeal);

        //2.新增套餐关联的菜品  --setmeal-dish
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        setmealDishes.forEach(setmealDish -> {
            setmealDish.setSetmealId(setmeal.getId());
        });
        setMealDishMapper.addMutipleDishInSetmealDish(setmealDishes);
    }

    /**
     * 分页条件查询
     * @param setmealPageQueryDTO
     * @return
     */
    @Override
    public PageResult page(SetmealPageQueryDTO setmealPageQueryDTO) {
        PageHelper.startPage(setmealPageQueryDTO.getPage(),setmealPageQueryDTO.getPageSize());

        Setmeal setmeal = BeanHelper.copyProperties(setmealPageQueryDTO, Setmeal.class);
        List<SetmealVO> setmealList = setmealMapper.list(setmeal);
        Page<SetmealVO> page = (Page<SetmealVO>) setmealList;

        return new PageResult(page.getTotal(),page.getResult());
    }

    /**
     * 批量删除套餐
     * @param ids
     */
    @Transactional
    @Override
    public void deleteSetmeal(List<Long> ids) {
        //1.起售的套餐不能删除
        //1.1查询套餐起售的数量
        Long count  = setmealMapper.countSetmealEnableByIds(ids);
        if (count > 0){
            throw new BusinessException(MessageConstant.DISH_ON_SALE);
        }

        //2.删除套餐表中的
        setmealMapper.deleteSetmealByIds(ids);

        //3.删除关联表中的
        setMealDishMapper.deleteSetmealWithDishByIds(ids);
    }

    /**
     * 根据id查询套餐
     * @param id
     * @return
     */
    @Transactional
    @Override
    public SetmealVO findSetmealById(Long id) {
        Setmeal setmeal = Setmeal.builder().id(id).build();
        //1.查询到菜品基本字段
        setmeal = setmealMapper.findSetmealById(setmeal);

        //2.根据套餐id查询对应的菜品信息
        List<SetmealDish> setmealDishes = setMealDishMapper.findDishFormDishSetmeal(id);

        //将菜品信息封装到套餐表中
        SetmealVO setmealVO = BeanHelper.copyProperties(setmeal, SetmealVO.class);
        setmealVO.setSetmealDishes(setmealDishes);
        return setmealVO;
    }

    /**
     * 修改套餐
     * @param setmealDTO
     */
    @Override
    @Transactional
    public void update(SetmealDTO setmealDTO) {
        //1.修改基本字段
        Setmeal setmeal = BeanHelper.copyProperties(setmealDTO, Setmeal.class);
        setmealMapper.updateSetmeal(setmeal);

        //2.修改菜品字段 --setmeal_dish
        //2.1删除菜品字段
        Long id = setmealDTO.getId();
        setMealDishMapper.deleteSetmealWithDishByIds(Collections.singletonList(id));

        //2.2新增菜品字段
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        setmealDishes.forEach(setmealDish -> {
            setmealDish.setSetmealId(setmealDTO.getId());
        });

        setMealDishMapper.addMutipleDishInSetmealDish(setmealDishes);
    }

    /**
     * 套餐的起售/停售
     * @param status
     * @param id
     */
    @Override
    @Transactional
    public void updateSetmealStatus(Integer status, Long id) {
        //1.起售套餐时候，套餐下的所有菜品都需要起售
        //1.1修改套餐的状态
        Setmeal setmeal = Setmeal.builder().id(id).status(status).build();
        setmealMapper.updateSetmeal(setmeal);

        //1.2修改套餐下的所有菜品
        //1.2.1查询到套餐菜品关联表
        List<SetmealDish> setmealDishs = setMealDishMapper.findDishFormDishSetmeal(id);

        ArrayList<Long> ids = new ArrayList<>();
        for (SetmealDish setmealDish : setmealDishs) {
            Long dishId = setmealDish.getDishId();
            ids.add(dishId);
        }
        Long count = dishMapper.countEnableDishByIds(ids);

            if(count != ids.size()){
                throw new BusinessException(MessageConstant.SETMEAL_ENABLE_FAILED);
            }
        //2.停售套餐不管菜品
        setmealMapper.updateSetmeal(setmeal);
    }

    /**
     * 根据套餐id查询菜品
     * @param id
     * @return
     */
    @Override
    @Transactional
    public List<DishItemVO> findDishFromSetmealById(Long id) {
        //查询到套餐下的菜品
        List<SetmealDish> setmealDishes = setMealDishMapper.findDishFormDishSetmeal(id);
        List<DishItemVO> dishItemVOS = setmealDishes.stream().map(setmealDish -> {
            Dish dish = dishMapper.findDishById(setmealDish.getDishId());
            return DishItemVO.builder().copies(setmealDish.getCopies()).image(dish.getImage())
                    .name(dish.getName())
                    .description(dish.getDescription()).build();
        }).collect(Collectors.toList());
        return dishItemVOS;
    }

}
