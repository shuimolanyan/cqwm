package com.sky.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersSubmitDTO;
import com.sky.entity.AddressBook;
import com.sky.entity.OrderDetail;
import com.sky.entity.Orders;
import com.sky.entity.ShoppingCart;
import com.sky.exception.BusinessException;
import com.sky.mapper.AddressBookMapper;
import com.sky.mapper.OrderDetailMapper;
import com.sky.mapper.OrdersMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.properties.BaiduMapProperties;
import com.sky.result.Location;
import com.sky.result.PageResult;
import com.sky.service.OrdersService;
import com.sky.utils.BaiduMapUtil;
import com.sky.utils.BeanHelper;
import com.sky.utils.HttpClientUtil;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import com.sky.webSocket.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrdersServiceImpl implements OrdersService {

    @Value("${sky.address.destination}")
    private String destination;

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private AddressBookMapper addressBookMapper;

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    @Autowired
    private OrderDetailMapper orderDetailMapper;

    @Autowired
    private BaiduMapProperties baiduAddressProperties;

    @Autowired
    private WebSocketServer webSocketServer;

    @Autowired
    private BaiduMapUtil baiduMapUtil;
    @Override
    @Transactional
    public OrderSubmitVO submitOrders(OrdersSubmitDTO ordersSubmitDTO) throws IOException {
        //0.查询购物车商品信息，用于组装订单详情表信息
        ShoppingCart shoppingCart = ShoppingCart.builder()
                .userId(BaseContext.getCurrentId())
                .build();
        List<ShoppingCart> shoppingCarts = shoppingCartMapper.list(shoppingCart);

        //0.查询地址信息，用户组装订单表
        AddressBook addressBook = AddressBook.builder()
                .id(ordersSubmitDTO.getAddressBookId())
                .userId(BaseContext.getCurrentId())
                .build();
        List<AddressBook> addressBooks = addressBookMapper.list(addressBook);
        AddressBook address = addressBooks.get(0);

        //0.1判断是否在配送范围内
        if(address != null){
            Location shopLocation = baiduMapUtil.getAddressGeo(destination);


            StringBuilder stringBuilder = new StringBuilder();
            String origin = stringBuilder.append(address.getProvinceName())
                    .append(address.getCityName())
                    .append(address.getDistrictName())
                    .append(address.getDetail()).toString();

            Location originLocation = baiduMapUtil.getAddressGeo(origin);


            Integer distance = baiduMapUtil.getDistance(originLocation, shopLocation);
//        Long distance = countDistance(address);
            log.info("距离为{}",distance);
            if(distance >= MessageConstant.DISTANCE){
                throw new BusinessException(MessageConstant.DELIVERY_OUT_OF_RANGE);
            }
        }


        //1.提交数据到订单列表
        if(address == null){
            throw new BusinessException(MessageConstant.ADDRESS_BOOK_IS_NULL);
        }
        Orders orders = BeanHelper.copyProperties(ordersSubmitDTO, Orders.class);
        orders.setNumber(String.valueOf(System.nanoTime()));
        orders.setStatus(orders.ORDER_STAUTS_PENDING_PAYMENT);
        orders.setUserId(BaseContext.getCurrentId());
        orders.setOrderTime(LocalDateTime.now());
        orders.setPayStatus(orders.PAY_STATUS_UN_PAID);
        orders.setPhone(address.getPhone());
        orders.setAddress(address.getDetail());
        orders.setConsignee(address.getConsignee());
        orders.setUserName(address.getConsignee());

        ordersMapper.insert(orders);

        log.info("订单id{}",orders.getId());
        //2.提交数据到订单详情表
        if(CollectionUtils.isEmpty(shoppingCarts)){
            throw new BusinessException(MessageConstant.SHOPPING_CART_IS_NULL);
        }
        List<OrderDetail> orderDetails = shoppingCarts.stream().map(cart -> {
            OrderDetail orderDetail = BeanHelper.copyProperties(cart, OrderDetail.class);
            orderDetail.setOrderId(orders.getId());
            return orderDetail;
        }).collect(Collectors.toList());


        orderDetailMapper.insert(orderDetails);


        //下单后提醒商家接单
        HashMap<Object, Object> messageMap = new HashMap<>();
        messageMap.put("type",1);
        messageMap.put("orderId",orders.getId());
        messageMap.put("content","订单号"+orders.getNumber());

        webSocketServer.sendMsg(JSONObject.toJSONString(messageMap));

        //3.组装返回数据
        OrderSubmitVO orderSubmitVO = OrderSubmitVO.builder().orderAmount(orders.getAmount())
                .orderNumber(orders.getNumber())
                .orderTime(orders.getOrderTime())
                .build();

        //4.订单完成要删除购物车中的商品
        for (ShoppingCart cart : shoppingCarts) {
            shoppingCartMapper.delete(cart);
        }



        return orderSubmitVO;
    }



    /**
     * 查询历史订单
     * @return
     */
    @Override
    public PageResult findHistoryOrders(OrdersPageQueryDTO ordersPageQueryDTO) {
        PageHelper.startPage(ordersPageQueryDTO.getPage(),ordersPageQueryDTO.getPageSize());

        Orders orders = BeanHelper.copyProperties(ordersPageQueryDTO, Orders.class);
        orders.setUserId(BaseContext.getCurrentId());
        List<OrderVO> orderVOs = ordersMapper.page(orders);

        Page<OrderVO> p = (Page<OrderVO>) orderVOs;

        return new PageResult(p.getTotal(),p.getResult());
    }

    /**
     * 根据id查询订单信息
     * @param id
     * @return
     */
    @Override
    public OrderVO findOrderDeatilById(Long id) {
        Orders orders = Orders.builder().id(id).build();
        OrderVO orderVO = ordersMapper.list(orders);
        return orderVO;
    }

    /**
     * 取消订单
     * @param id
     */
    @Override
    @Transactional
    public void cancelOrder(Long id) {
        Orders orders = Orders.builder()
                .id(id)
                .build();
        OrderVO orderVO = ordersMapper.list(orders);
        //1.判断订单是否存在
        if(orderVO == null){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }

        //2.检验订单状态是否可以被取消，仅代付款和待接单可以取消
        if(orderVO.getStatus() == 1 || orderVO.getStatus() == 2){
            //3.修改订单状态
            orders = Orders.builder().id(id).status(6).build();
            ordersMapper.update(orders);
        }


        //4.如果订单状态为待接单且支付状态为已支付，还需进行退款操作
        if(orderVO.getStatus() == 2 && orderVO.getPayStatus() == 1){
            orders = Orders.builder().id(id).payStatus(2).build();
            ordersMapper.update(orders);
        }

    }

    /**
     * 再次购买
     * @param id
     */
    @Override
    @Transactional
    public void onceBy(Long id) {
        //1.根据id查询该订单
        Orders orders = Orders.builder()
                .id(id)
                .userId(BaseContext.getCurrentId())
                .build();
        OrderVO orderVO = ordersMapper.list(orders);
        if (orderVO == null){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }

        //2.得到该订单内的商品
        List<OrderDetail> orderDetailList = orderVO.getOrderDetailList();

        List<ShoppingCart> shoppingCarts = orderDetailList.stream().map(orderDetail -> {
            ShoppingCart shoppingCart = BeanHelper.copyProperties(orderDetail, ShoppingCart.class);
            shoppingCart.setUserId(BaseContext.getCurrentId());
            shoppingCart.setCreateTime(LocalDateTime.now());
            return shoppingCart;
        }).collect(Collectors.toList());


        //批量添加
        shoppingCartMapper.insertMultiple(shoppingCarts);
    }

    /**
     * 催单
     * @param id
     */
    @Override
    public void reminderOrder(Long id) throws IOException {
        Orders orders = Orders.builder().id(id).userId(BaseContext.getCurrentId()).build();
        OrderVO orderVO = ordersMapper.list(orders);

        HashMap<Object, Object> messageMap = new HashMap<>();
        messageMap.put("type",2);
        messageMap.put("orderId",id);
        messageMap.put("content","订单号"+orderVO.getNumber());

        webSocketServer.sendMsg(JSONObject.toJSONString(messageMap));
    }

}
