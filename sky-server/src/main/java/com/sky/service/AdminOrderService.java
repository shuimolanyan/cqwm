package com.sky.service;

import com.sky.dto.OrdersCancelDTO;
import com.sky.dto.OrdersConfirmDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersRejectionDTO;
import com.sky.result.PageResult;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;

public interface AdminOrderService {
    //条件分页查询
    PageResult list(OrdersPageQueryDTO ordersPageQueryDTO);

    //统计各个状态的订单的数量
    OrderStatisticsVO countOderStatusNumber();

    //查询订单详情
    OrderVO findOrderDetailById(Long id);

    //接单
    void receiveOrder(OrdersConfirmDTO ordersConfirmDTO);

    //拒单
    void rejectOrder(OrdersRejectionDTO ordersRejectionDTO);

    //取消订单
    void cancelOrder(OrdersCancelDTO ordersCancelDTO);

    //派送订单
    void deliveryOrder(Long id);

    //完成订单
    void susccessOrder(Long id);

}
