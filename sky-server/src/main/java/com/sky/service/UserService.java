package com.sky.service;

import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;

public interface UserService {
    //C端用户登录，若是第一次则保存
    User login(UserLoginDTO userLoginDTO);

}
