package com.sky.service;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;

import java.util.List;

public interface CategoryService {

    //分页查询
    PageResult page(CategoryPageQueryDTO categoryPageQueryDTO);

    //根据id查询分类
    Category findCategoryById(Long id);

    //修改分类
    void updateCategory(CategoryDTO categoryDTO);

    //修改，禁用/启用
    void updateStatus(Integer status, Long id);

    //新增分类
    void insetCategory(CategoryDTO categoryDTO);

    //删除分类
    void deleteCategory(Long id);

    //根据类型查询分类
    List<Category> findCategoryByType(CategoryDTO categoryDTO);

    //C端的（根据类型）查询分类
    List<Category> findCategoryByTypeOnC(Integer type);

    //根据分类id查询套餐
    List<Setmeal> findSetmealByCategoryId(Long id);
}
