package com.sky.task;

import com.sky.constant.MessageConstant;
import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Component
public class OrdersTask {

    @Autowired
    private OrdersMapper ordersMapper;
    /**
     * 自动取消十五分钟前未支付的订单
     */
    @Scheduled(cron = "* 15 * * * ?")
    public void autoCancelOrder(){
        //1.先查询符合条件(超过十五分钟并且未支付)的订单
        LocalDateTime before15Time = LocalDateTime.now().minusMinutes(15);
        List<Orders> ordersList = ordersMapper.findOrdersByTime(Orders.ORDER_STAUTS_PENDING_PAYMENT,before15Time );

        if(!CollectionUtils.isEmpty(ordersList)){
            ordersList.stream().forEach(orders -> {
                //2.取消订单
                orders.setStatus(Orders.ORDER_STAUTS_CANCELLED);
                orders.setCancelReason(MessageConstant.ORDER_IS_OVERTIME);
                orders.setCancelTime(LocalDateTime.now());

                ordersMapper.update(orders);
            });
        }
    }

    /**
     * 没天凌晨一点检查一次是否存在派送中的订单，如果存在则修改状态为已完成
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void autoCompleteOrder(){
        //1.先查询符合条件(超过两小时并且派送中)的订单
        LocalDateTime before2Time = LocalDateTime.now().minusHours(2);
        List<Orders> ordersList = ordersMapper.findOrdersByTime(Orders.ORDER_STAUTS_TO_BE_CONFIRMED,before2Time );

        if(!CollectionUtils.isEmpty(ordersList)){
            ordersList.stream().forEach(orders -> {
                //2.完成订单
                orders.setStatus(Orders.ORDER_STAUTS_COMPLETED);
                orders.setDeliveryTime(LocalDateTime.now());

                ordersMapper.update(orders);
            });
        }
    }
}
