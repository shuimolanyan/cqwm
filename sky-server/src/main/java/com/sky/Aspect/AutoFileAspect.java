package com.sky.Aspect;

import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

import static com.sky.constant.AutoFillConstant.*;

@Aspect
@Slf4j
@Component
public class AutoFileAspect {

    @Before("execution(* com.sky.mapper.*.*(..)) && @annotation(com.sky.Aspect.AutoFile)")
    public void autoFile(JoinPoint joinPoint) throws Exception {
        //1.获取需要新增或修改的方法的参数
        Object[] args = joinPoint.getArgs();
        if(ObjectUtils.isEmpty(args)){
            return;
        }

        Object obj = args[0];
//        Method[] declaredMethods = obj.getClass().getDeclaredMethods();
//        for (Method declaredMethod : declaredMethods) {
//            AutoFile annotation = declaredMethod.getAnnotation(AutoFile.class);
//        }

        //2.利用反射为其赋值
        Method updateTime = obj.getClass().getDeclaredMethod(SET_UPDATE_TIME, LocalDateTime.class);
        Method createTime = obj.getClass().getDeclaredMethod(SET_CREATE_TIME, LocalDateTime.class);
        Method updateUser = obj.getClass().getDeclaredMethod(SET_UPDATE_USER, Long.class);
        Method createUser = obj.getClass().getDeclaredMethod(SET_CREATE_USER, Long.class);

        //3.获取注解的value属性
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        AutoFile autoFile = method.getAnnotation(AutoFile.class);
        OperationType operationType = autoFile.value();

        //4.判断需要增强的方法是增加还是修改
        if(operationType.equals(OperationType.INSERT)){
            createUser.invoke(obj, BaseContext.getCurrentId());
            createTime.invoke(obj,LocalDateTime.now());
        }
        updateTime.invoke(obj,LocalDateTime.now());
        updateUser.invoke(obj,BaseContext.getCurrentId());
    }

    /**
     * 第二中
     */
//    @Before("execution(* com.sky.mapper.*.*(..)) && @annotation(autoFile)")
    public void autoFile(JoinPoint joinPoint , AutoFile autoFile) throws Exception {
        //1.获取参数
        Object[] args = joinPoint.getArgs();
        if(ObjectUtils.isEmpty(args)){
           return;
        }
        Object obj = args[0];

        //2.给想要增强的方法，利用反射赋值
        Method updateTime = obj.getClass().getDeclaredMethod(SET_UPDATE_TIME,LocalDateTime.class);
        Method createTime = obj.getClass().getDeclaredMethod(SET_CREATE_TIME,LocalDateTime.class);
        Method updateUser = obj.getClass().getDeclaredMethod(SET_UPDATE_USER,Long.class);
        Method createUser = obj.getClass().getDeclaredMethod(SET_CREATE_USER,Long.class);

        //3. 得到注解的值
        OperationType operationType = autoFile.value();


        //5.判断
        if(operationType.equals(OperationType.INSERT)){
            createTime.invoke(obj,LocalDateTime.now());
            createUser.invoke(obj,BaseContext.getCurrentId());
        }
        updateTime.invoke(obj,LocalDateTime.now());
        updateUser.invoke(obj,BaseContext.getCurrentId());

    }
}

