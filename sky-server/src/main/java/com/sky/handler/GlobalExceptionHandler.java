package com.sky.handler;

import com.sky.constant.MessageConstant;
import com.sky.exception.BaseException;
import com.sky.exception.DataException;
import com.sky.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static com.sky.constant.MessageConstant.UNKNOWN_ERROR;
import static com.sky.constant.MessageConstant.USERNAME_EXISTS;

/**
 * 全局异常处理器，处理项目中抛出的业务异常
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 捕获业务异常
     * @param ex
     * @return
     */
    @ExceptionHandler
    public Result baseExceptionHandler(BaseException ex){
    	ex.printStackTrace();
        log.error("异常信息：{}", ex.getMessage());
        return Result.error(ex.getMessage());
    }

    /**
     * 捕获重复输入异常
     */
    @ExceptionHandler
    public Result duplicateKeyHandler(DuplicateKeyException ex){
        ex.printStackTrace();
        String message = ex.getCause().getMessage();

        if(message.contains("Duplicate entry")){
            String msg = message.split(" ")[2];
            return Result.error(msg+USERNAME_EXISTS);
        }else {
            return Result.error(UNKNOWN_ERROR);
        }
    }


    /**
     * 捕获其他异常
     * @param ex
     * @return
     */
    @ExceptionHandler
    public Result exceptionHandler(Exception ex){
    	ex.printStackTrace();
        log.error("异常信息：{}", ex.getMessage());
        return Result.error(UNKNOWN_ERROR);

    }

}
